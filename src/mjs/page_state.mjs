import {global} from "./global.mjs";
import * as util from "./util.mjs";

export class PageState {
    constructor(defaultState) {
        this._default = defaultState;
        this._internal = {};
        this._external = undefined;
        this._merged = undefined;
        this._updateMerged();
    }

    _updateMerged() {
        let newMerged;
        if (this._external === undefined) {
            newMerged = {
                ...this._default,
                ...this._internal,
            };
        }
        else {
            newMerged = {
                ...this._default,
                ...this._external,
                ...this._internal,
            };
        }
        const unchanged = util.deepEquals(this._merged, newMerged);
        if (unchanged) {
            return false;
        }

        this._merged = newMerged;
        return true;
    }

    setExternal(externalState, relativeContextPath) {
        if (relativeContextPath) {
            this._external = util.getPathValueArray(externalState, relativeContextPath);
        }
        else {
            this._external = externalState;
        }

        if (typeof(this._external) === 'string') {
            const key = this._external;
            this._external = {};
            this._external[key] = true;
        }

        // Return true if changed,
        // so Element knows to propagate changes
        return this._updateMerged();
    }

    setInternal(internalState) {
        this._internal = internalState;

        if (typeof(this._internal) === 'string') {
            const key = this._internal;
            this._internal = {};
            this._internal[key] = true;
        }

        // Return true if changed,
        // so Element knows to propagate changes
        return this._updateMerged();
    }

    get value() {
        return this._merged;
    }
}
