export function isObject(v) {
    if (v === null) return true;
    if (Array.isArray(v)) return true;
    if (typeof(v) === 'object') return true;

    return false;
}


export function matchFilter(state, posFilter) {
    console.assert(state, 'matchFilter: no state', state, posFilter);
    if (posFilter === null
        || posFilter === undefined
        || Object.keys(posFilter).length == 0)
    {
        //console.log('empty filter');
        return true;
    }

    if (state === posFilter) {
        return true;
    }

    if (!state) {
        return false;
    }

    if (Object.keys(state).length == 0) {
        //console.log('empty state');
        return false;
    }

    if (typeof(state) !== typeof(posFilter)) {
        if (typeof(posFilter) === 'string') {
            return (posFilter in state);
        }

        return false;
    }

    /// Check if the KVPs in posFilter all appear in the currentState
    ///  posFilter RHS can be an array of acceptable values
    for(const[key, val] of Object.entries(posFilter)) {
        //const currentVal = currentState[key];
        if (val === '*') {
            // Allow wildcard; mainly so modals can clear parameters in getDeactivateFilter
            continue;
        }
        const currentVal = getPathValue(state, key);
        if (Array.isArray(val)) {
            if (!val.includes(currentVal)) return false;
        }
        else if (currentVal === null) {
            if (val !== null) {
                return false;
            }
        }
        else if (typeof(currentVal) === 'object') {
            if (!matchFilter(currentVal, val)) {
                return false;
            }
        }
        else if (currentVal !== val) {
            return false;
        }
    }
    return true;
}


export function parsePageFilterStr(pageFilterStr) {
    if (!pageFilterStr) return null;
    const parsedVal = parseStrVal(pageFilterStr);

    //console.warn('PARSE PAGE FILTER STR', pageFilterStr);
    if (typeof(parsedVal) === 'string') {
        // "edit.id.*" --> {"edit": {"id": "*"}}
        // "edit.*" --> {"edit": "*"}
        // "edit" --> "edit"
        const arrayVal = parsedVal.split('.');
        return parsePageFilterArray(arrayVal);
    }
    return parsedVal;
}


export function parsePageFilterArray(arrayVal) {
    const lastIndex = arrayVal.length - 1;

    // End with the last element of the array
    let node = parseStrVal(arrayVal[lastIndex]);
    for (let i=lastIndex-1; i>=0; i--) {
        const key = arrayVal[i];
        const outerNode = {};
        outerNode[key] = node;
        node = outerNode;
    }

    return node;
}


export function pathStrToArray(strVal) {
    if (!strVal) return [];
    return strVal.split('.');
}


export function filterState(state, posFilter) {
    // Could refactor matchFilter
    //  to just check if filterState returns anything
    //  but it would be less efficient (but more robust)
    if (state === undefined) return undefined;
    if (posFilter === undefined) return undefined;
    if (posFilter === null) {
        if (state === null) return null;
        return undefined;
    }

    const stateType = typeof(state);
    const filterType = typeof(posFilter);

    if (filterType === 'string') {
        if (posFilter === '*') {
            return state;
        }
        else if (stateType === 'string') {
            if (state === posFilter) {
                return posFilter;
            }
        }
        else {
            if (posFilter in state) {
                return posFilter;
            }
        }
        return undefined;
    }

    const matchingNode = {};
    for(const[key, filterVal] of Object.entries(posFilter)) {
        const stateVal = state[key];
        if (stateVal === undefined) {
            continue;
        }

        const subMatch = filterState(stateVal, filterVal);
        if (subMatch !== undefined) {
            matchingNode[key] = subMatch;
        }
    }
    return matchingNode;
}
// console.log('filterState test:', filterState(
//     {'a': 1}, {'a': '*'}
// ));
// console.log('filterState test:', filterState(
//     {'a': {'b': 1}}, {'a': '*'}
// ));
// console.log('filterState test:', filterState(
//     {'a': {'b': 1}}, {'a': 'b'}
// ));
// console.log('filterState test:', filterState(
//     {'a': {'b': 1}}, {'a': 'c'}
// ));
// console.log('filterState test:', filterState(
//     {'a': {'b': 1}}, {'c': 2}
// ));

export function localeCompare(a, b) {
    if (a === null) return 1;
    if (b === null) return -1;
    if (a === null && b === null) return 0;

    const result = a - b;

    if (isNaN(result)) {
        return a.toString().localeCompare(b);
    }
    else {
        return result;
    }
}


export function deepCopy(obj) {
    // const strVal = JSON.stringify(obj);
    // const jsonVal = JSON.parse(strVal);
    // return jsonVal;

    if (obj === undefined) return undefined;
    if (obj === null) return null;
    if (Array.isArray(obj)) {
        const returnVal = [];
        for (const v of obj) {
            returnVal.push(deepCopy(v));
        }
        return returnVal;
    }
    else if (typeof(obj) === 'object') {
        const returnVal = {};
        for (const [k, v] of Object.entries(obj)) {
            returnVal[k] = deepCopy(v);
        }
        return returnVal;
    }

    return obj;
}


export function deepAssign(dest, src, keepRefs=false, mask=false) {
    if (src === undefined) return dest;
    if (src === null) return dest;

    if (typeof(src) !== 'object') {
        console.error(`deepAssign: src is not object:`, src);
        return;
    }
    if (typeof(dest) !== 'object') {
        console.error(`deepAssign: dest is not object:`, src);
        return;
    }

    for (const[key, srcVal] of Object.entries(src)) {
        const destVal = dest[key];

        // Option to mask/limit assignment to items that already exist in dest
        if (mask && destVal === undefined) {
            continue;
        }

        if (destVal === null) {
            if (keepRefs) dest[key] = srcVal;
            else dest[key] = deepCopy(srcVal);
        }
        else if (typeof(destVal) === 'object') {
            deepAssign(destVal, srcVal, keepRefs, mask);
        }
        else {
            if (keepRefs) dest[key] = srcVal;
            else dest[key] = deepCopy(srcVal);
        }
    }

    return dest;
}


export function deepEquals(objA, objB, ignoreKeys=undefined) {
    /// Deep value equality comparison of any type
    if (objA === null) return (objB === null);
    if (objB === null) return (objA === null);
    if (objA === undefined) return (objB === undefined);
    if (objB === undefined) return (objA === undefined);
    if (objA === objB) return true;

    const objTypeA = typeof(objA);
    const objTypeB = typeof(objB);
    if (objTypeA !== objTypeB) {
        return false;
    }
    else if (objTypeA === 'object') {
        const keysA = Object.keys(objA);
        const keysB = Object.keys(objB);

        if (ignoreKeys) {
            if (Array.isArray(ignoreKeys)) {
                for (const ignoreKey of ignoreKeys) {
                    removeValueFromArray(ignoreKey, keysA);
                    removeValueFromArray(ignoreKey, keysB);
                }
            }
            else {
                removeValueFromArray(ignoreKeys, keysA);
                removeValueFromArray(ignoreKeys, keysB);
            }
        }

        if (keysA.length !== keysB.length) {
            return false;
        }

        const allKeys = [...keysA, ...keysB].filter(uniqueFilter);
        if (allKeys.length !== keysA.length) {
            return false;
        }
        
        for (let k=0; k<allKeys.length; k++) {
            const key = allKeys[k];
            const subA = objA[key];
            const subB = objB[key];
            if (!deepEquals(subA, subB)) {
                return false;
            }
        }
    }
    else if (objTypeA === 'array') {
        if (objA.length !== objB.length) {
            return false;
        }
        for (let a=0; a<objA.length; a++) {
            const itemA = objA[a];
            const itemB = objB[a];
            if (!deepEquals(itemA, itemB)) {
                return false;
            }
        }
    }
    else {
        const c = (objA === objB);
        return c;
    }
    return true;
}


export function replaceLeafNodes(obj, replacement, inPlace=true) {
    if (obj === undefined) return replacement;
    if (obj === null) return replacement;
    if (typeof(obj) !== 'object') {
        return replacement;
    }

    const returnObj = inPlace ? obj : {};
    for (const [key, val] of Object.entries(obj)) {
        if (typeof(val) === 'object') {
            if (inPlace) {
                replaceLeafNodes(obj[key], replacement, true);
            }
            else {
                returnObj[key] = replaceLeafNodes(obj[key], replacement, false);
            }
        }
        else {
            returnObj[key] = replacement;
        }
    }

    return returnObj;
}


export function makeDiffTree(objA, objB, ignoreKeys=undefined) {
    // Returns either:
    //  a tree that only includes the keys of changed values, and true for the leaf values
    //  or, in the case of direct value comparisons:
    //   true: if value is different
    //   false: if value is same

    let typeObjA = typeof(objA);
    let typeObjB = typeof(objB);

    if (objA === null) typeObjA = 'null';
    else if (objA === undefined) typeObjA = 'undefined';

    if (objB === null) typeObjB = 'null';
    else if (objB === undefined) typeObjB = 'undefined';

    // Entire LHS object has changed
    if (typeObjA !== typeObjB) {
        if (typeObjA === 'object') {
            return replaceLeafNodes(objA, true, false);
        }

        // Entire RHS object is new (therefore changed)
        if (typeObjB === 'object') {
            return replaceLeafNodes(objB, true, false);
        }

        return true;
    }

    if (ignoreKeys) {
        if (!Array.isArray(ignoreKeys)) {
            ignoreKeys = [ignoreKeys];
        }
    }

    if (typeObjA === 'object') {
        const keysA = Object.keys(objA);
        const keysB = Object.keys(objB);
        const allKeys = [...keysA, ...keysB].filter(uniqueFilter);

        if (ignoreKeys) {
            for (const ignoreKey of ignoreKeys) {
                removeValueFromArray(ignoreKey, allKeys);
            }
        }

        const returnTree = {};
        let changeCount = 0;
        for (const key of allKeys) {
            const subA = objA[key];
            const subB = objB[key];
            const subTree = makeDiffTree(subA, subB, ignoreKeys);
            if (subTree) {
                changeCount++;
                returnTree[key] = subTree;
            }
        }
        if (changeCount) {
            return returnTree;
        }
        else {
            return false;
        }
    }

    return (objA !== objB);
}


export function makeDelta(objFrom, objTo) {
    console.assert(typeof(objTo) === 'object');
    console.assert(typeof(objFrom) === 'object');

    const delta = {
        'add': {}, // added
        'mod': {}, // modified
        'rem': {}, // removed
    }

    for (const [key, fromVal] of Object.entries(objFrom)) {
        const toVal = objTo[key]
        if (toVal === undefined) {
            delta.rem[key] = fromVal;
        }
        else if (!deepEquals(fromVal, toVal)) {
            delta.mod[key] = toVal;
        }
    }

    for (const [key, toVal] of Object.entries(objTo)) {
        const fromVal = objFrom[key]
        if (fromVal === undefined) {
            delta.add[key] = toVal;
        }
    }

    return delta;
}


/**
 * Returns a hash code from a string
 * @param  {String} str The string to hash.
 * @return {Number}    A 32bit integer
 * @see http://werxltd.com/wp/2010/05/13/javascript-implementation-of-javas-string-hashcode-method/
 */
export function hashCode(str) {
    if (!str) return 0;

    let hash = 0;
    for (let i = 0, len = str.length; i < len; i++) {
        let chr = str.charCodeAt(i);
        hash = (hash << 5) - hash + chr;
        hash |= 0; // Convert to 32bit integer
    }
    return hash;
}


function uniqueFilter(value, index, self) {
    return self.indexOf(value) === index;
}


export function removeValueFromArray(val, array, all=false) {
    let index = array.indexOf(val);
    while (index > -1) {
        array.splice(index, 1);
        if (all) {
            index = array.indexOf(val);
        }
        else {
            index = -1;
        }
    }
    return array; // Optional
}


export function getPathValue(obj, pathStr, defaultVal=undefined) {
    if (!pathStr) {
        return obj;
    }

    const pathArray = pathStr.split('.');
    return getPathValueArray(obj, pathArray, defaultVal);
}


export function getPathValueArray(obj, pathArray, defaultVal=undefined) {
    if (!obj) {
        return defaultVal;
    }
    if (!pathArray) {
        return obj;
    }

    let val = obj;
    for (let p=0; p<pathArray.length; p++) {
        const key = pathArray[p];
        if (typeof(val) !== 'object') {
            return defaultVal;
        }
        val = val[key];
        if (val === undefined) {
            return defaultVal;
        }
    }

    return val;
}

// function removeUndefined(o) {
//   let stack = [o], i;
//   while(stack.length) {
//     Object.entries(i=stack.pop()).forEach(([k,v])=>{
//       if(v===undefined) delete i[k];
//       if(v instanceof Object) stack.push(v);
//     })
//   }
//   return o;
// }

export function setPathValue(obj, pathStr, val) {
    // if (!obj) {
    //     return undefined;
    // }
    if (obj === null || typeof(obj) !== 'object') {
        console.error(`Attempting setPathValue on non-object`, obj, pathStr, val);
        return null;
    }

    if (!pathStr) {
        return setPathValueArray(obj, null, val);
    }

    const pathArray = pathStr.split('.');
    return setPathValueArray(obj, pathArray, val);
}


export function setPathValueArray(obj, pathArray, val) {
    if (!pathArray) {
        pathArray = [];
    }

    let node = obj;
    const pathLength = pathArray.length;

    if (typeof(node) !== 'object') {
        console.warn(`cannot set path value on`, node, pathArray);
        return;
    }

    for (let p=0; p<pathLength-1; p++) {
        const key = pathArray[p];
        let nextNode = node[key];
        if (nextNode === undefined) {
            nextNode = {};
            node[key] = nextNode;
        }
        else if (typeof(nextNode) !== 'object') {
            console.warn(`converting node from ${nextNode} to object`, obj, key, pathArray);
            nextNode = {};
            node[key] = nextNode;
        }
        node = nextNode;
    }

    if (pathLength > 0) {
        const finalKey = pathArray[pathLength-1];
        if (val === undefined) {
            delete node[finalKey];
        }
        else {
            node[finalKey] = val;
        }
    }
    else {
        //console.assert(typeof(val) === 'object', 'Attempting to assign non-object at root of object');
        Object.assign(obj, val);
    }

    return obj;
}


export function getOrSetPathValue(obj, pathStr, defaultFn) {
    const existingValue = getPathValue(obj, pathStr);
    if (existingValue !== undefined) {
        return existingValue;
    }

    const newValue = new defaultFn();
    setPathValue(obj, pathStr, newValue)

    return newValue;
}


export function parseStrVal(strVal) {
    if (typeof(strVal) !== 'string') {
        return strVal;
    }

    try {
        const jsonVal = JSON.parse(strVal);
        return jsonVal;
    }
    catch(e) {
    }

    // if (!isNaN(+strVal)) {
    //     return parseFloat(strVal);
    // }

    const lVal = strVal.toLowerCase();
    if (lVal === 'null') return null;
    if (lVal === 'true') return true;
    if (lVal === 'false') return false;

    return strVal;
}


export function getElementValue(el, allowJson=false) {
    if (el.matches('[type="checkbox"]')) {
        return el.checked;
    }
    else if (el.matches('fa-check-button')) {
        return el.checked;
    }
    const val = el.value;
    if (val === undefined) return undefined;

    const jsonVal = parseStrVal(el.value);
    const dataType = typeof(jsonVal);
    const isJson = (dataType === 'object' || dataType === 'array');
    if (!allowJson && isJson) {
        return val;
    }
    else {
        return jsonVal;
    }
}


export function setElementValue(el, val, defaultVal) {
    if (val === undefined) {
        val = defaultVal;
    }

    if (val === undefined) {
        return;
    }

    if (el.matches('[type="checkbox"]')) {
        el.checked = val;
    }
    else if (el.matches('fa-check-button')) {
        el.checked = val;
    }
    else {
        el.value = val;
    }
}


export function resetElementValue(el) {
    if (el.matches('select')) {
        el.selectedIndex = 0;
    }
    else {
        el.value = '';
    }    
}


export function strToJsonSafe(val) {
    try {
        return JSON.parse(val);
    }
    catch(e) { }
    return val;
}


export function strToArray(strVal) {
    if (strVal === '' || strVal === undefined || strVal === null) {
        return [];
    }

    let arrayVal = strToJsonSafe(strVal);
    if (!Array.isArray(arrayVal)) {
        return [arrayVal];
    }

    return arrayVal;
}


export function strSimilarity(s1, s2) {
    // JaroWrinker
    var m = 0;

    // Exit early if either are empty.
    if (s1.length === 0 || s2.length === 0) {
        return 0;
    }

    // Exit early if they're an exact match.
    if (s1 === s2) {
        return 1;
    }

    const range = (Math.floor(Math.max(s1.length, s2.length) / 2)) - 1;
    const s1Matches = new Array(s1.length);
    const s2Matches = new Array(s2.length);

    for (let i = 0; i < s1.length; i++) {
        const low  = (i >= range) ? i - range : 0;
        const high = (i + range <= s2.length) ? (i + range) : (s2.length - 1);

        for (let j = low; j <= high; j++) {
            if (s1Matches[i] !== true && s2Matches[j] !== true && s1[i] === s2[j]) {
                m++;
                s1Matches[i] = s2Matches[j] = true;
                break;
            }
        }
    }

    // Exit early if no matches were found.
    if (m === 0) {
        return 0;
    }

    // Count the transpositions.
    let n_trans = 0;
    let k = 0;

    for (let i = 0; i < s1.length; i++) {
        if (s1Matches[i] === true) {
            let j = k;
            while (j < s2.length) {
                if (s2Matches[j] === true) {
                    k = j + 1;
                    break;
                }
                j++;
            }

            if (s1[i] !== s2[j]) {
                n_trans++;
            }
        }
    }

    let weight = (m / s1.length + m / s2.length + (m - (n_trans / 2)) / m) / 3;
    let l      = 0;
    let p      = 0.1;

    if (weight > 0.7) {
        while (s1[l] === s2[l] && l < 4) {
            l++;
        }
        weight = weight + l * p * (1 - weight);
    }

    return weight;
}


export function JsonToUrlEncodedStr(obj) {
    var str = '';
    var hasPrev = false;
    for (const [key, val] of Object.entries(obj)) {
        // Omit null values altogether
        if (val === null) {
            continue;
        }

        if (hasPrev) str += '&';
        else hasPrev = true;

        // Either convert the object to a string, otherwise encode the string directly
        const dataType = typeof(val);
        const strVal = (dataType === 'object' || dataType === 'array')
                    ? encodeURIComponent(JSON.stringify(val))
                    : encodeURIComponent(val);
        str += `${key}=${strVal}`;
    }
    return str;
}


export function urlEncodedStrToJson(strVal) {
    if (strVal.length > 2) {
        const jsonStr = '{"' + strVal.replace(/&/g, '","').replace(/=/g,'":"') + '"}';
        const jsonData = JSON.parse(
            jsonStr,
            function(key, value) {
                // return top-level value
                if (key === '') return value;

                const strValue = decodeURIComponent(value);
                return strToJsonSafe(strValue);
            }
        );
        return jsonData;
    }
    return {};
}


/// Clean up a url string (trim trailing slash) and append query params
export function toUrl(urlStr, params) {
    if (!urlStr) urlStr = '';

    // Remove the leading and trailing slash (potentially results in empty string)
    //let strVal = '/' + urlStr.replaceAll(/^\/|\/$/g, '');
    //if (strVal === '') strVal = '/';

    // Append the encoded parameters
    if (params) {
        urlStr += '?' + JsonToUrlEncodedStr(params);
    }

    return urlStr;
}


export function kvpObjToArray(obj) {
    const maxIndex = Math.max(...Object.keys(obj));
    const returnArray = Array(maxIndex + 1);
    for (const [key, val] of Object.entries(obj)) {
        returnArray[parseInt(key)] = val;
    }
    return returnArray;
}


export function pageStackToHashStr(pageStack) {
    //const hashStr = btoa(JsonToUrlEncodedStr(pageStack));
    const hashStr = JsonToUrlEncodedStr(pageStack);
    return hashStr;
}


export function hashStrToPageStack(hashStr) {
    //const stateStackDict = urlEncodedStrToJson(atob(hashStr));
    const pageStackDict = urlEncodedStrToJson(hashStr);
    const pageStack = kvpObjToArray(pageStackDict);
    return pageStack;
}


export function pageStateToHashStr(pageState) {
    if (pageState === undefined || pageState === null) {
        pageState = {};
    }
    const hashStr = JsonToUrlEncodedStr(pageState);
    return hashStr;
}


export function hashStrToPageState(hashStr) {
    const pageState = urlEncodedStrToJson(hashStr);
    return pageState;
}


export async function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

export function encodeHashKey(object) {
    const objectStr = JsonToUrlEncodedStr(object);

    let hash = 0;
    if (objectStr.length == 0) {
        return hash;
    }
    for (let i=0; i<objectStr.length; i++) {
        const char = objectStr.charCodeAt(i);
        hash = ((hash << 5) - hash) + char;
        hash = hash & hash;
    }
 
    return hash;
}

export function prettySqlDate(sqlDate) {
    const datePattern = /(\d{4})-(\d{2})-(\d{2}).(\d{2}):(\d{2}):(\d{2})/;
    const d = datePattern.exec(sqlDate);
    return `${d[1]}-${d[2]}-${d[3]} ${d[4]}:${d[5]}:${d[6]} UTC`;
}

// const text =
//   "An obscure body in the S-K System, your majesty. The inhabitants refer to it as the planet Earth.";

// async function digestMessage(message) {
//   const encoder = new TextEncoder();
//   const data = encoder.encode(message);
//   const hash = await crypto.subtle.digest("SHA-256", data);
//   return hash;
// }

// digestMessage(text).then((digestBuffer) =>
//   console.log(digestBuffer.byteLength),
// );
