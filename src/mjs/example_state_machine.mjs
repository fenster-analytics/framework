import {ElementStateMachine} from "./element_state_machine.mjs";
import {ElementState} from "./element_state.mjs";
import * as util from "./util.mjs";

//
// 1. Load data from the API
//
class Load extends ElementState {
    _init() {
        // Optionally set the state's default template to render
        // (simple loading spinner in this case)
        this.template = 'fa_loading';

        // States can be reused in other StateMachines though
        // and templates can be overridden in the StateMachine config
    }

    async _start() {
        // Normally this would be loaded from an API
        this.shared.rawData = [
            { category: "Fruits", price: "$1", stocked: true, name: "Apple" },
            { category: "Fruits", price: "$1", stocked: true, name: "Dragonfruit" },
            { category: "Fruits", price: "$2", stocked: false, name: "Passionfruit" },
            { category: "Vegetables", price: "$2", stocked: true, name: "Spinach" },
            { category: "Vegetables", price: "$4", stocked: false, name: "Pumpkin" },
            { category: "Vegetables", price: "$1", stocked: true, name: "Peas" }
        ];

        // When the API call completes, continue to the next state
        this.goto('render');
    }
}

//
// 2. Render the main template and enable functionality once the data has loaded
//
class Render extends ElementState {
    _init() {
        this.template = 'docs_example';
    }

    async _start() {
        this._productList = this.shared.rawData;
        this._updateList();
    }

    filterName(el) {
        this._filterStr = el.value;
        this._updateList();
    }

    filterStock(el) {
        // getElementValue converts checkbox value to true/false
        this._inStockOnly = util.getElementValue(el);
        this._updateList();
    }

    _updateList() {
        const categoryDict = {};
        for (const product of this._productList) {
            // Skip out-of-stock products
            if (this._inStockOnly && !product.stocked) {
                continue;
            }

            // Skip names that don't pass filter
            if (this._filterStr) {
                if (!product.name?.toLowerCase().includes(this._filterStr)) {
                    continue;
                }
            }

            // Logic should generally happen outside of the template
            product.displayClass = product.stocked ? 'in-stock' : 'not-in-stock';

            // Get or create the list for products in this category
            const category = util.getOrSetPathValue(categoryDict, product.category, Array);
            category.push(product);
        }
        this.shared.categoryDict = categoryDict;
        this._renderDynamicContent();
    }
}

//
// StateMachine is the actual Element. It:
//   * defines the state mapping
//   * overrides templates if necessary
//   * sets the initial activation
//     (the 'load' state in this case)
//
export class ExampleStateMachine extends ElementStateMachine {
    constructor() {
        super({
            'load': {
                'active': true,
                'class': Load,
                'config': {
                    //'template': 'fancy_loading_screen',
                },
            },
            'render': {
                'class': Render,
            },
        });
    }
}
window.customElements.define('fa-docs-example-statemachine', ExampleStateMachine);