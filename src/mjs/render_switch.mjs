import {FormElement} from "./form_element.mjs";
import * as util from "./util.mjs";

// This is the same as RenderGroup except
//   _scrapeValue should only get the _root (no need to requery)

export class RenderSwitch extends FormElement {
    constructor() {
        super('fa_render_wrapper');
        this._initOptionList();
        this._childElementType = 'fa-render-target';
    }

    get shared() {
        return this.hel.contextElement.shared;
    }

    _initOptionList() {
        if (this._optionList !== undefined) {
            return;
        }

        this._optionList = [];
        for (const el of this._slotChildren) {
            const template = el.getAttribute('data-fa-template');

            let pageFilter = el.getAttribute('data-fa-page-filter');
            if (pageFilter) {
                pageFilter = util.parsePageFilterStr(pageFilter);
            }
            else {
                pageFilter = null;
            }

            let valueFilter = el.getAttribute('data-fa-value-filter');
            if (valueFilter) {
                valueFilter = util.parseStrVal(valueFilter);
            }
            else {
                valueFilter = null;
            }

            const optionDef = {
                'pageFilter': pageFilter,
                'valueFilter': valueFilter,
                'template': template,
                'idx': this._optionList.length,
                'innerHTML': el.innerHTML,
                'noCache': el.hasAttribute('no-cache'),
            };
            this._optionList.push(optionDef);
        }

        // Free the child elements
        this._slotChildren = null;
    }

    setTemplateAndRender(t) {
        console.error(`cannot set template directly on:`, this, t);
    }

    _onPageStateChanged() {
        if (!this._connected) {
            return;
        }

        this.active = true;
        this._renderOption();

        // Propagate pageState to new children
        super._onPageStateChanged();
    }

    getManualPageStateElements() {
        return [];
    }

    getValueElements() {
        return [];
    }

    getFormElements() {
        if (this._currentOption) {
            return [this._currentOption.el];
        }
        return [];
    }

    _renderOption() {
        console.assert(this._rendered, `RenderSwitch: renderInternal called on unrendered element`, this);
        const pendingOption = this._findActiveOption();
        this._setCurrentOption(pendingOption);
    }

    _findActiveOption() {
        // Find the first matching option
        for (const optionDef of this._optionList) {
            if (this.evalFilter(optionDef.pageFilter, optionDef.valueFilter)) {
                return optionDef;
            }
        }
        // @todo: This is actually fine,
        // but would be nice to wait to find the first element
        console.warn('RenderSwitch: no active option found', this.pageState, this._value, this);
    }

    _setCurrentOption(optionDef) {
        // Deactivate all instances (or just current option el)
        for (const el of this._shadowRoot.querySelectorAll(this._childElementType)) {
            el.active = false;
        }

        if (!optionDef) {
            this._currentOption = null;
            return;
        }

        // Cache previously rendered options
        if (optionDef.noCache) {
            let bRelevantPageStateChanged = true;
            if (optionDef.relevantPageState) {
                // Check if the relevant page state is the same as before
                const bUnchanged = util.matchFilter(this.pageState, optionDef.relevantPageState);
                bRelevantPageStateChanged = !bUnchanged;
            }

            // Remove the previous element and redraw
            if (bRelevantPageStateChanged) {
                //console.warn('CHANGED', this.pageState, optionDef.relevantPageState);
                if (optionDef.el) {
                    optionDef.el.remove();
                    optionDef.el = null;
                }
            }
        }

        // Create the option's element, if it doesn't exist
        if (!optionDef.el) {
            optionDef.el = document.createElement(this._childElementType);
            optionDef.el.id = `i${optionDef.idx}`;
            if (optionDef.template) {
                optionDef.el.template = optionDef.template;
            }
            this._shadowRoot.appendChild(optionDef.el);
        }

        // Construct relevant pageState and save it
        optionDef.relevantPageState = util.filterState(this.pageState, optionDef.pageFilter);
        //console.log('FILTER STATE', this.pageState, optionDef.pageFilter, optionDef.relevantPageState);

        this._currentOption = optionDef;
        optionDef.el.active = true;
    }

    _renderDynamicContent() {
        // The value (from the root) at the context for this Element
        this._value = this.getRootValue(this.hel.valueContext);

        // For debugging
        //this.setAttribute('data-fa-value', JSON.stringify(this._value));

        if (!this._rendered) {
            // No reason to update content that doesn't exist yet
            console.warn('renderDynamicContent: element not rendered yet', this);
            return;
        }

        this._renderOption();

        // Notify child elements that they may want to re-render
        for (const el of this.getFormElements()) {
            // @todo: A fast way to look up whether a context has changes
            el._renderDynamicContent();
        }
    }
}
window.customElements.define('fa-render-switch', RenderSwitch);