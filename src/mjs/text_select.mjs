import {Element} from "./element.mjs";
import * as util from "./util.mjs";

export class TextSelect extends Element {
	constructor() {
		super('fa_text_select');
		this._index = -1;
		this.items = util.strToArray(this.getAttribute('data-fa-item-list'));

		this._root.addEventListener('change', (e) => {
            if (e.target === this.el.input) {
                this.dispatchEvent(new Event("change", {bubbles: true, cancelable: true}));    
            }
        });
	}

	set value(v) {
		this._value = v;
		this._originalValue = v;
		this.el.input.value = v;
	}
	get value() {
		return this.el.input.value;
	}
	set disabled(d) {
		this.el.input.disabled = d;
	}
	get disabled() {
		return this.el.input.disabled;
	}

	set items(itemArray) {
		this._items = itemArray;
		// TODO: Apply filter
		this.shared.filteredItems = this._items;

		// Pick the currently selected item
		if (this._index < 0) {
			for (let idx=0; idx<this.shared.filteredItems.length; idx++) {
				const strVal = this.shared.filteredItems[idx];
				if (this._value === strVal) {
					this._index = idx;
					break;
				}
			}
		}

		if (this._rendered) {
			this._renderDynamicContent();
		}
	}

	selectOption(idx) {
		if (idx >= 0 && idx < this.shared.filteredItems.length) {
			this._value = this.shared.filteredItems[idx];
            const previousValue = util.getElementValue(this.el.input);
			this.el.input.value = this._value;
            if (this._value !== previousValue) {
                this.dispatchEvent(new Event("change", {bubbles: true, cancelable: true}));
            }
		}
	}

	previewOption(idx) {
		if (idx >= 0 && idx < this.shared.filteredItems.length) {
			this.el.input.value = this.shared.filteredItems[idx];
		}
	}

	updateIndex() {
		// Clamp
		if (this._index < -1) {
			this._index = -1;
		}
		else if (this._index >= this.shared.filteredItems.length) {
			this._index = this.shared.filteredItems.length - 1;
		}

		// Highlight
		for (const el of this.el.list.querySelectorAll(`option`)) {
			el.classList.remove('active');
		}
		const optionEl = this.el.list.querySelector(`[data-idx="${this._index}"]`);
		if (optionEl) {
			optionEl.classList.add('active');
		}
	}

	toggleList() {
		if (this.el.list.classList.contains('active')) {
			this.el.list.classList.remove('active');
		}
		else {
			this.el.list.classList.add('active');
			const optionEl = this.el.list.querySelector(`[data-idx="${this._index}"]`);
			if (optionEl) optionEl.scrollIntoView({
				block: "nearest", inline: "nearest", behavior: "smooth",
			});
		}
	}

	findClosestListValue() {
		if (!this._value) {
			return;
		}

		// Find and select the most similar string in the list
		let bestWeight = 0;
		let bestIdx = -1;
		for (let idx=0; idx<this.shared.filteredItems.length; idx++) {
			const strVal = this.shared.filteredItems[idx];
			const weight = FA.util.strSimilarity(this._value, strVal);
			if (weight > bestWeight) {
				bestIdx = idx;
				bestWeight = weight;
			}
		}

		// Set and highlight the best index
		if (bestIdx > -1) {
			this._index = bestIdx;
			this.updateIndex();
		}
	}

	connectedCallback() {
		super.connectedCallback();
		this._root.addEventListener('mousedown', (e) => {
			if (e.srcElement.hasAttribute('data-idx')) {
				this._index = parseInt(e.srcElement.getAttribute('data-idx'));
				this.updateIndex();
				this.selectOption(this._index);
			}
			else {
				this.toggleList();
			}
		});
		this.el.input.addEventListener('input', (e) => {
			this.el.list.classList.add('active');
			this._value = this.el.input.value;
			this.findClosestListValue();
		});
		this.el.input.addEventListener('keydown', (e) => {
			if (e.keyCode === 27) { // Esc
				if (this.el.list.classList.contains('active')) {
					// Reset to last the input value
					this.el.list.classList.remove('active');
					this.el.input.value = this._value;
				}
				else {
					// Reset to the original input value
					this.el.input.value = this._originalValue;
				}
				return;
			}
			if (e.keyCode === 35 || e.keyCode === 36) { // Home, End
				return;
			}
			if (e.keyCode === 16 || e.keyCode === 17) { // Shift, Ctrl
				return;
			}
			if (e.keyCode === 13) { // Enter
				e.preventDefault();
				this.selectOption(this._index);
				this.el.list.classList.remove('active');
				return;
			}

			if (e.keyCode === 40) { // Down
				e.preventDefault();
				if (this.el.list.classList.contains('active')) {
					this._index++;
					this.updateIndex();
					this.previewOption(this._index);
				}
				else {
					this.el.list.classList.add('active');
				}
			}
			else if (e.keyCode === 38) { // Up
				e.preventDefault();
				if (this.el.list.classList.contains('active')) {
					this._index--;
					this.updateIndex();
					this.previewOption(this._index);
				}
				else {
					this.el.list.classList.add('active');
				}
			}
		});
		this.el.input.addEventListener('focus', (e) => {
			this.el.list.classList.add('active');
		});
		this.el.input.addEventListener('blur', (e) => {
			this.el.list.classList.remove('active');
		});
        this.el.input.addEventListener('change', (e) => {
            this.dispatchEvent(new Event("change", {bubbles: true, cancelable: true}));
        });
	}

	_renderDynamicContent() {
		super._renderDynamicContent();
		this.updateIndex();
	}
}
window.customElements.define('fa-text-select', TextSelect);