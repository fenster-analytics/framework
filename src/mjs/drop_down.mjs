import {Element} from "./element.mjs"

export class DropDown extends HTMLElement {
    constructor(template) {
        super();

        this.el = {};
        this.el.title = this.querySelector('fa-drop-down-title');
        this.el.content = this.querySelector('fa-drop-down-content');

        this.addEventListener('mouseenter', e => {
            this.show();
        });
        this.addEventListener('mouseleave', e => {
            this.hide();
        });

        // Toggle on click
        this.el.title.addEventListener('click', e => {
            this.toggle();
        });
    }

    connectedCallback() {
        if (!this.hasAttribute('data-fa-manual-close')) {
            // Optional window click listener to close on external click
            const self = this;
            this._closeOnExternalClick = function(e) {
                const clickPath = e.composedPath();
                for (const el of clickPath) {
                    const isSelf = (el === self);
                    if (isSelf) {
                        return;
                    }
                }
                self.forceHide();
            };
            document.addEventListener('click', this._closeOnExternalClick);
        }
    }

    disconnectedCallback() {
        document.removeEventListener('click', this._closeOnExternalClick);
    }

    show() {
        const contentLocation = this.getAttribute('data-fa-content-location');
        const rect = this.getBoundingClientRect();

        // Render first to get the width of the content, then position it
        this.el.content.classList.add('active');

        const contentWidth = this.el.content.scrollWidth;

        switch(contentLocation) {
            case 'right':
                this.el.content.style.left = rect.right + 'px';
                this.el.content.style.top = rect.top + 'px';
                break;
            case 'left':
                this.el.content.style.left = (rect.left - contentWidth) + 'px';
                this.el.content.style.top = rect.top + 'px';
                break;
            case 'bottom-left':
                this.el.content.style.left = (rect.right - contentWidth) + 'px';
                this.el.content.style.top = rect.bottom + 'px';
                break;
            case 'bottom':
            default:
                this.el.content.style.left = rect.left + 'px';
                this.el.content.style.top = rect.bottom + 'px';
        }
    }

    hide() {
        if (this._on) {
            return;
        }
        this.el.content.classList.remove('active');
    }

    forceHide() {
        this._on = false;
        this.classList.remove('toggled-on');
        this.el.content.classList.remove('active');
    }

    toggle() {
        this._on = !this._on;
        if (this._on) {
            this.show();
            this.classList.add('toggled-on');
        }
        else {
            this.classList.remove('toggled-on');
            this.hide();
        }
    }
}
window.customElements.define('fa-drop-down', DropDown);