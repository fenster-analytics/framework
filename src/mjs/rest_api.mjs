import * as util from "./util.mjs";

const batchIntervalMS = 500;
let globalBlockingCount = 0;

export class RestAPI {
    constructor(href, config) {
        console.log('RestAPI', href, config);
        // Switch this to include the url directly in the constructor
        this._baseUrl = new URL(href);
        this._config = config;
        this._blockingCount = 0;
        this._token = null;
        //let prefix = config.prefix ? config.prefix : '';
        //prefix = prefix.replaceAll(/^\/|\/$/g, '');
        //this._baseUrl = new URL(prefix, `${config.protocol}//${config.host}:${config.port}`);
        console.log('REST API BASE URL', this._baseUrl);
        // if (!this._config.headers) {
        //     this._config.headers = null;
        // }
    }

    set token(t) {
        // This is a bit of a hack (only allows one token at the moment)
        this._token = t;
    }

    get token() {
        return this._token;
    }

    extend(postfixRoute) {
        // Create an API copy with a route appended
        const newUrl = this._baseUrl.href.replaceAll(/\/$/g, '') + '/' + postfixRoute.replaceAll(/^\//g, '');
        const newRestApi = new RestAPI(newUrl, this._config);
        return newRestApi;
        // This unfortunately drops everything other than the domain of the baseURL
        // const newUrl = new URL(postfixRoute, this._baseUrl);
        // const newRestApi = new RestAPI(newUrl.href, this._config);
        // return newRestApi;

        // const URL_Join = (...args) =>
        //   args
        //     .join('/')
        //     .replace(/[\/]+/g, '/')
        //     .replace(/^(.+):\//, '$1://')
        //     .replace(/^file:/, 'file:/')
        //     .replace(/\/(\?|&|#[^!])/g, '$1')
        //     .replace(/\?/g, '&')
        //     .replace('&', '?');
        // console.log(URL_Join('http://www.google.com', 'a', '/b/cd', '?foo=123', '?bar=foo'));
    }

    createUrl(relPath, includeToken=false) {
        let requestPath = '';
        if (relPath) {
            requestPath = relPath.replaceAll(/^\//g, '');
        }

        const url = new URL(this._baseUrl);
        url.pathname = url.pathname.replaceAll(/\/$/g, '') + '/' + requestPath;

        let href = url.href;
        if (includeToken) {
            const header = {'x-api-key': this._token};
            const headerStr = new URLSearchParams({'headers': JSON.stringify(header)});
            href += `?${headerStr}`;
        }

        return href;
    }

    async exec(request, block=false) {
        if (block) {
            globalBlockingCount++;
        }
        if (globalBlockingCount > 0) {
            console.log('ENABLE BLOCKING LAYER');
            document.documentElement.setAttribute(`data-blocking`, globalBlockingCount);
        }

        let requestPath = '';
        // if (request.path) {
        //     requestPath = request.path.replaceAll(/^\/|\/$/g, '');
        // }
        if (request.path) {
            requestPath = request.path.replaceAll(/^\//g, '');
        }

        const url = new URL(this._baseUrl);
        url.pathname = url.pathname.replaceAll(/\/$/g, '') + '/' + requestPath;

        // // TODO: Add to scheduler
        // // Exec immediately if most recent request was greater than some time ago
        // // Add to the batch otherwise
        // // todo: return fetch
        // // TODO: Allow overriding headers?
        // // Object.assign
        const fetchParams = {
            'method': request.method,
        };

        if (this._config.headers) {
            fetchParams.headers = this._config.headers;
        }
        else {
            fetchParams.headers = {};
        }

        // Hack to include a project token
        if (this._token) {
            fetchParams.headers['x-api-key'] = this._token;
        }

        if (request.body) {
            fetchParams.body = JSON.stringify(request.body);
        }
        if (request.file) {
            console.assert(!request.body, `Post File with body currently not implemented`);
            fetchParams.body = new FormData();
            fetchParams.body.append('file', request.file, request.file.name);

            // Hack: pass in headers as query params, since fetch needs to auto-generate the real headers
            if (!request.params) request.params = {};
            request.params.headers = JSON.stringify(fetchParams.headers);

            // Remove the real headers so that fetch will generate them to match the form data            
            delete fetchParams.headers;
        }

        let href = url.href;
        if (request.params) {
            const queryParams = new URLSearchParams(request.params);
            href += `?${queryParams}`;
        }

        console.log('RestAPI::exec:', request.method, href);
        try {
            const response = await fetch(href, fetchParams);
            const result = await response.json();
            if (!response.ok) {
                if (result && result.detail) {
                    throw new Error(result.detail);
                }
                else {
                    throw new Error(response.body);
                }
            }

            // Hack: Update the local project token
            this._token = response.headers.get('x-api-key');

            return result;
        }
        catch(e) {
            throw e;
        }
        finally {
            if (block) {
                globalBlockingCount--;
            }
            console.assert(globalBlockingCount >= 0);

            if (globalBlockingCount === 0) {
                document.documentElement.removeAttribute(`data-blocking`);
            }
        }
    }

    async get(path, params=null, block=false) {
        return this.exec({
            'method': 'GET',
            'path': path,
            'params': params,
        }, block);
    }

    async put(path, params=null, body=null, block=false) {
        return this.exec({
            'method': 'PUT',
            'path': path,
            'params': params,
            'body': body,
        }, block);
    }

    async post(path, params=null, body=null, block=false) {
        return this.exec({
            'method': 'POST',
            'path': path,
            'params': params,
            'body': body,
        }, block);
    }

    async postFile(path, params=null, file=null, block=false) {
        return this.exec({
            'method': 'POST',
            'path': path,
            'params': params,
            'file': file,
        }, block);
    }

    async delete(path, params=null, block=false) {
        return this.exec({
            'method': 'DELETE',
            'path': path,
            'params': params,
        }, block);
    }
}