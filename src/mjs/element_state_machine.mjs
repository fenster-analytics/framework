import {BitSet} from "bitset";

import {FormElement} from "./form_element.mjs";
import {ElementState} from "./element_state.mjs";
import * as util from "./util.mjs";

export const defaultStateMachineDef = {
    'init': {
        'active': true,
        'class': ElementState,
        'config': {
            'template': 'fa_loading',
        }
    },
};

export class ElementStateMachine extends FormElement {
    constructor(stateMachineDef, defaultPageState) {
        super(undefined, defaultPageState);

        this._isRunning = null;

        // The raw map of transition rules and state properties
        this._stateMachineDef = stateMachineDef;
        this._updateStatesHandle = null;
        this._errorState = undefined;
    }

    set template(t) {
        console.error(`cannot set template directly on ElementStateMachine`, t, this);
    }

    connectedCallback() {
        this._loadStateMachineDef();
        super.connectedCallback();
    }

    onActivate() {
        super.onActivate();
        this._run();
    }

    get elementState() {
        return this._activeStateMap;
    }

    set errorState(key) {
        this._errorState = key;
    }

    _loadStateMachineDef() {
        this._current = new BitSet();
        this._pending = new BitSet();

        // Map the state name to a local instance of the state data
        this._stateMap = {};
        this._activeStateMap = {};
        this._stateArray = [];
        this._stateTrackMap = {};

        // Instantiate the local state instances
        let stateIndex = 0;
        for (const[key, stateDef] of Object.entries(this._stateMachineDef)) {
            const stateClass = stateDef.class ? stateDef.class : ElementState;
            const stateTrack = stateDef.track ? stateDef.track : 'default';
            const stateInstance = new stateClass(key, stateIndex, this, stateDef.config, stateTrack);

            // State classes must be derived from the ElementState base class
            console.assert(stateInstance instanceof ElementState);

            this._stateMap[key] = stateInstance;
            this._activeStateMap[key] = false;
            this._stateArray.push(stateInstance);

            // Sort into tracks (typically for killing all states per track)
            this._getStateTrack(stateTrack).set(stateInstance);
            stateIndex++;
        }

        // Activate the default-active states
        for (const[key, state] of Object.entries(this._stateMachineDef)) {
            if (state.active) {
                this.spawnState(key);
            }
        }
    }

    _getStateTrack(trackName) {
        if (!this._stateTrackMap.hasOwnProperty(trackName)) {
            this._stateTrackMap[trackName] = new BitSet();
        }
        return this._stateTrackMap[trackName];
    }

    get renderContext() {
        return {
            'elementState': this._activeStateMap,
            ...super.renderContext
        }
    }

    async _run() {
        if (this._isRunning) {
            return;
        }
        this._isRunning = true;

        const minFrameMS = 17;
        let iteration = 0;
        while (this._isRunning) {
            //console.log('SM: loop', iteration, this);
            try {
                const hasChanges = await this._updateStates();
                if (!hasChanges) {
                    //console.log('SM: Terminated', iteration, this);
                    this._isRunning = false;
                    return;
                }
            }
            catch (e) {
                console.error('StateMachine caught error:', e);
                if (this._errorState) {
                    this.shared.tMsg = { 
                        message: e.message, 
                        stack: e.stack ? e.stack.split('\n') : null,
                        name: e.name,
                        cause: e.cause,
                    };
                    this.goto(this._errorState);
                    // Allow one state change to handle the error
                    try {
                        await this._updateStates();
                    }
                    catch (eSub) {
                        console.error(eSub);
                    }
                }
                this._isRunning = false;
                return;
            }

            if (iteration > 20) {
                console.error('SM: Max iterations reached', iteration, this);
                this._isRunning = false;
                return;
            }

            iteration++;
            await util.sleep(minFrameMS);
        }
    }

    async _updateStates() {
        // Allow a subsequent update to be scheduled from any of the state logic
        this._updateStatesHandle = null;

        if (!this._active) {
            // @todo: this is probably fine (just getting scheduled right before deactivating an element)
            console.warn('SM: updateStates on inactive element', this);
        }

        //console.log('updateStates current', this._current.clone().toArray());
        //console.log('updateStates pending', this._pending.clone().toArray());

        const stopSet = this._current.and(this._pending.not());
        const startSet = this._pending.and(this._current.not());

        // Copy pending into current
        this._current = this._pending.clone();

        const stopIndexList = stopSet.toArray();
        const startIndexList = startSet.toArray();
        const updateIndexList = this._current.toArray();
        
        //console.log('SM: stop list', stopIndexList, this);
        for (const stateIndex of stopIndexList) {
            const state = this._stateArray[stateIndex];
            this._activeStateMap[state._key] = false;
            await state._stop();
        }
        //await Promise.all([

        for (const stateIndex of startIndexList) {
            const state = this._stateArray[stateIndex];
            this._activeStateMap[state._key] = true;
        }

        // Pick a new template (optional) and render it
        let newTemplate = undefined;
        let bShouldRender = false;
        for (const stateIndex of startIndexList) {
            const state = this._stateArray[stateIndex];
            const template = state.template;
            if (template) {
                newTemplate = template;
                bShouldRender = state.forceRender || (template !== this._template);
                break;
            }
        }

        if (bShouldRender) {
            this.setTemplateAndRender(newTemplate);
        }
        
        //console.log('SM: start list', startIndexList, this);
        for (const stateIndex of startIndexList) {
            const state = this._stateArray[stateIndex];
            await state._start();
        }

        // Updating doesn't really make sense without render frames
        //console.log('SM: update list', updateIndexList, this);
        for (const stateIndex of updateIndexList) {
            const state = this._stateArray[stateIndex];
            await state._update();
        }

        // TODO: Better inspection for dev/debug
        //console.log('SM: state machine stats', this.getStats());

        const hasChanges = !this._pending.equals(this._current);
        return hasChanges;
    }

    _execFunction(fnName, target) {
        // see Element::_execFunction
        let execCount = 0;
        const returnArray = [];

        // Trigger the function by name, pass in this as the instigator
        if (this[fnName] !== undefined) {
            const fn = this[fnName].bind(this);
            const result = fn(target);
            if (result !== undefined) {
                execCount++;
                returnArray.push(result);
            }
        }

        // Allow each active state to handle function calls (mainly to call click and change functions)
        const updateIndexList = this._current.toArray();
        for (const stateIndex of updateIndexList) {
            const state = this._stateArray[stateIndex];
            const result = state._execFunction(fnName, target);
            if (result !== undefined) {
                execCount++;
                returnArray.push(result);
            }
        }

        if (execCount === 0) {
            return undefined;
        }

        return returnArray[0];
    }

    spawnState(key) {
        const state = this._stateMap[key];
        if (!state) {
            console.error(`Undefined state: ${key}`);
            return;
        }
        //console.log('SM: spawnState', key, state, this._current.clone().toArray());
        this._pending.set(state._index, 1);
        // this._requestUpdate();
    }

    killState(key) {
        const state = this._stateMap[key];
        if (!state) {
            console.error(`Undefined state: ${key}`);
            return;
        }
        //console.log('SM: killState', key, state, this._current.clone().toArray(), this._pending.clone().toArray());

        this._pending.set(state._index, 0);
        //console.log('SM: killed', this._current.clone().toArray(), this._pending.clone().toArray());
        //this._requestUpdate();
    }

    killAllStates(track=undefined) {
        //console.log('SM: killAllStates', track);
        if (track) {
            const trackSet = this._getStateTrack(track);
            this._pending = this._pending.and(trackSet.not());
        }
        else {
            this._pending.clear();
        }
        //this._requestUpdate();
    }

    goto(key) {
        //console.warn('SM: goto', key);
        this.killAllStates();
        this.spawnState(key);
        this._requestUpdate();
    }

    _requestUpdate() {
        if (this._isRunning) {
            return;
        }

        // const noChanges = this._pending.equals(this._current);
        // if (noChanges) {
        //     console.log('REQUEST UPDATE: NO CHANGES');
        //     return;
        // }

        // Queue a run after the current execution context completes
        setTimeout(() => {this._run()}, 0);
    }

    getStats() {
        const stateStats = {};
        for (const state of this._stateArray) {
            stateStats[state._key] = state.getStats();
        }
        return stateStats;
    }
}
