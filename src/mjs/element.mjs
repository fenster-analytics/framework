// Use global commonJS jotun-client until mjs version is ready
//import * as Jotun2 from "@keyrock-dev/jotun-client";
//import * as hb from 'handlebars';

import {ElementMixIn} from "./element_mixin.mjs";
import * as util from "./util.mjs";

const cssList = [];

const _cssMap = {};
var _globalStyleSheets = [];

const MAX_CHANGE_COUNT = 3;

export const contextType = {
    value: Symbol('value'),
    group: Symbol('group'),
    list: Symbol('list'),
    dict: Symbol('dict'),
};

// var style = document.createElement("style");
// document.head.appendChild(style); // must append before you can access sheet property
// var sheet = style.sheet;

export class Element extends ElementMixIn(HTMLElement) {
    static registerCSS(key, cssFile) {
        console.log('registerCSS', key, cssFile);
        for (const cssSrc of document.styleSheets) {
            if (cssSrc.href.endsWith(cssFile)) {
                // Manually copy the rules
                //  this sucks, because it loses the CSS debug mapping
                //  but it's much faster on the client
                //  (otherwise every element tries to parse the CSS link)
                const cssClone = new CSSStyleSheet();
                for (const srcRule of cssSrc.rules) {
                    cssClone.insertRule(srcRule.cssText);
                }
                _cssMap[key] = cssClone;
            }
        }

        _globalStyleSheets = Object.values(_cssMap);
    }

    static get observedAttributes() {
        return [
            ...super.observedAttributes,
            'data-fa-template',
            'value',
        ];
    }

    attributeChangedCallback(name, oldVal, newVal) {
        super.attributeChangedCallback(name, oldVal, newVal);
        if (name === 'data-fa-template') {
            this.setTemplateAndRender(newVal);
        }
        else if (name === 'value') {
            const jsonVal = util.parseStrVal(newVal);
            this.value = jsonVal;
        }
    }

    set template(t) {
        this.setAttribute('data-fa-template', t);
    }

    setTemplateAndRender(t) {
        this._template = t;

        if (!this._connected) {
            return;
        }

        // Re-render all content
        if (this._rendered) {
            this._rendered = false;
            this._renderStaticContent();
            this._renderDynamicContent();
        }
    }

    constructor(template, defaultPageState) {
        super(defaultPageState);

        this._template = template;
        this._rendered = false;

        // Shared variables exposed via renderContext
        // (usually for passing data between states in stateMachines)
        this._shared = this.hasAttribute('data-fa-init-shared') ? util.parseStrVal(this.getAttribute('data-fa-init-shared')) : {};

        // Keep a copy of the original content
        this._originalText = this.innerHTML;
        this._slotChildren = [];
        for (const el of this.children) {
            this._slotChildren.push(el);
        }

        this.innerHTML = '';
        this._root = this.attachShadow({mode: 'open'});
        this._root.adoptedStyleSheets = _globalStyleSheets;

        // Always keep a reference to the shadowRoot,
        // in case the 'root' is redirected to a sub-element
        this._shadowRoot = this._root;

        this._registerListeners();
    }

    _initAttributes() {
        super._initAttributes();
        this.setAttribute('is-fa-context', '');
    }

    _initHierarchy() {
        super._initHierarchy();
        this.hel.valueRoot = this._findValueRoot();

        if (this.hel.valueRoot) {
            let relativeValueContextStr = this.getAttribute('data-fa-bind-value');
            if (relativeValueContextStr === null) relativeValueContextStr = '';

            // The outer context of this element
            let parentContext = this.hel.contextElement ? this.hel.contextElement.hel.valueContext : [];

            // Allow relative pathing
            const upLevelMatches = relativeValueContextStr.match(/^\.+/);
            if (upLevelMatches) {
                const upLevel = upLevelMatches[0].length - 1;
                if (parentContext.length < upLevel) {
                    console.error(`${relativeValueContextStr} outside of parentContext:`, parentContext, this);
                    return;
                }

                // Discard the unused path
                parentContext = [...parentContext]; // (Make a copy of the context)
                parentContext.splice(0, upLevel);
                relativeValueContextStr = relativeValueContextStr.substring(upLevel + 1);
                // @todo: form_element still parses data-fa-bind-value,
                //  so it needs to stay
                //this.setAttribute('data-fa-bind-value', relativeValueContextStr);

                // Redirect the contextElement
                for (let i=0; i<upLevel; i++) {
                    this.hel.contextElement = this.hel.contextElement.hel.contextElement;
                }
            }

            // Split string into path array
            const relValueContextPath = relativeValueContextStr ?
                relativeValueContextStr.split('.') : [];

            // The absolute valueContext
            this.hel.valueContext = [
                ...parentContext,
                ...relValueContextPath
            ];

            // // If a value was set before attaching, propagate it now
            // if (this._value !== undefined) {
            //     this.value = this._value;
            // }
        }
    }

    _findValueRoot() {
        const needsRoot = this.hasAttribute('data-fa-bind-value')
                        || this.hasAttribute('data-fa-bind-key');
        if (needsRoot) {
            if (!this.hel.contextElement) {
                console.error(`cannot bind relative value without context`, this);
                return null;
            }
            return this.hel.contextElement._findValueRoot();
        }
        return this;
    }

    isValueRoot() {
        return (this.hel.valueRoot === this);
    }

    _renderDynamicContent() {
        console.assert(this._root, 'no root', this);

        if (!this._rendered) {
            // No reason to update content that doesn't exist yet
            console.warn('renderDynamicContent: element not rendered yet', this);
            return;
        }

        for (const el of this._root.querySelectorAll('[data-fa-bind-template]')) {
            const contextPath = el.getAttribute('data-fa-bind-template');
            const pathVal = util.getPathValue(this.renderContext, contextPath);

            // Check for value changes by comparing hash values (rather than duplicating objects)
            const strVal = JSON.stringify(pathVal);
            const hashVal = util.hashCode(strVal);
            if (el.hashVal !== hashVal) {
                el.hashVal = hashVal;
                el.innerHTML = el.cTemplate(pathVal);
            }
        }
    }

    getPageStateElements() {
        return this._root.querySelectorAll('[is-fa-element]');
    }

    getManualPageStateElements() {
        return this._root.querySelectorAll('[data-fa-page-filter]:not([is-fa-element])');
    }

    _updateActiveFromFilter() {
        super._updateActiveFromFilter();
        this._activateManualPageStateElements();
    }

    _activateManualPageStateElements() {
        // Manually activate non-FA-elements (rare)
        for (const el of this.getManualPageStateElements()) {
            const pageStateContext = el.getAttribute('data-fa-page-context');
            const localPageState = util.getPathValue(this.pageState, pageStateContext);

            const filterStr = el.getAttribute('data-fa-page-filter');
            const filterJson = util.parsePageFilterStr(filterStr);
            const active = util.matchFilter(localPageState, filterJson);
            if (active) {
                el.classList.add('active');
            }
            else {
                el.classList.remove('active');
            }
        }
    }

    _onPageStateChanged() {
        super._onPageStateChanged();

        // For debugging:
        // if (this._connected) {
        //     this.setAttribute('data-fa-page-state', JSON.stringify(this.pageState));
        // }

        // Notify all listening children
        //const e = new Event('page-state-change', {bubbles: false, composed: false});
        //this.dispatchEvent(e);
        for (const el of this.getPageStateElements()) {
            el.pageState = this.pageState;
        }
    }

    get renderContext() {
        return {
            // Static (only updates via re-render)
            'static': {
                'global': this.global.renderData,
                'type': this.tagName,
                'slot': this._originalText,
            },
            // Dynamic (updates via data-fa-bind-template or data-fa-bind-value)
            'dynamic': {
                'value': this._value,
                'shared': this.shared,
                'pageState': this.pageState,
            },
            'loader': this.global.loaderHTML,
        }
    }

    get visible() {
        return (this.offsetWidth && this.offsetHeight);
    }

    onActivate() {
        super.onActivate();

        if (!this._rendered) {
            // Render the structural HTML
            this._renderStaticContent();
            this._renderDynamicContent();
        }

        const e = new Event('activate', {bubbles: true, composed: false});
        this.dispatchEvent(e);
    }

    onDeactivate() {
        super.onDeactivate();

        const e = new Event('deactivate', {bubbles: true, composed: false});
        this.dispatchEvent(e);
    }

    _renderStaticContent() {
        console.assert(!this._rendered, 'EL: static content re-render', this._template, this);

        if (this._template === undefined) {
            const cTemplate = Jotun.compile(this._originalText);
            this._root.innerHTML = cTemplate(this.renderContext);
        }
        else {
            this._root.innerHTML = Jotun.render(this._template, this.renderContext);
        }

        for (const el of this._root.querySelectorAll('[data-fa-bind-template]')) {
            el.cTemplate = Jotun.compile(el.innerHTML);
            el.innerHTML = this.global.loaderHTML;
        }
        for (const el of this._root.querySelectorAll('[data-fa-element-type]')) {
            const cTemplate = Jotun.compile(el.innerHTML);
            const dataPath = el.hasAttribute('data-fa-bind-template') ? el.getAttribute('data-fa-bind-template') : '';
            const elType = el.getAttribute('data-fa-element-type');

            const newEl = document.createElement(elType);
            newEl.classList = el.classList;
            newEl.innerHTML = '[TEMPLATE]';
            newEl.setAttribute('data-fa-bind-template', dataPath);
            newEl.cTemplate = cTemplate;
            
            el.parentNode.replaceChild(newEl, el);
        }

        this.el = {};
        for (const el of this._root.querySelectorAll('[auto-register]')) {
            const key = el.id;
            this.el[key] = el;
        }

        this._rendered = true;

        // Bind to local client options
        for (const el of this._root.querySelectorAll('[data-fa-option-key]')) {
            // Load the value from localStorage
            const key = el.getAttribute('data-fa-option-key');
            const val = localStorage.getItem(`option-${key}`);
            if (val !== null) {
                el.value = val;
            }
            el.addEventListener("change", this.global.changeLocalOption);
        }
    }

    set value(v) {
        if (!util.deepEquals(v, this._value)) {
            this._value = v;
            this._updateActiveFromFilter();
            this._renderDynamicContent();
        }
    }

    get value() {
        return this._value;
    }

    // set dynamic(jsonVal) {
    //     if (!util.deepEquals(jsonVal, this._dynamic)) {
    //         this._dynamic = jsonVal;
    //         if (this._rendered) {
    //             this._renderDynamicContent();
    //         }
    //     }
    // }

    get shared() {
        return this._shared;
    }

    _registerListeners() {
        // Shortcut for binding onClick functions (without doing "this.getRootNode().host.[function]")
        this._shadowRoot.addEventListener('click', (event) => this._handleClickEvent(event));
        this._shadowRoot.addEventListener('change', (event) => this._handleChangeEvent(event));
        this._shadowRoot.addEventListener('keyup', (event) => this._handleKeyupEvent(event));
    }

    _handleClickEvent(event) {
        // Auto click function
        const el = event.target;
        if (el.hasAttribute('click-function')) {
            const fnName = el.getAttribute('click-function');
            this._execFunction(fnName, event.target);
        }
    }

    _handleKeyupEvent(event) {
        // Auto keyup function
        const el = event.target;
        if (el.hasAttribute('keyup-function')) {
            const fnName = el.getAttribute('keyup-function');
            this._execFunction(fnName, event.target);
        }
    }

    _handleChangeEvent(event) {
        // Auto change function
        const el = event.target;
        if (el.hasAttribute('change-function')) {
            const fnName = el.getAttribute('change-function');
            this._execFunction(fnName, el);
        }
    }

    _execFunction(fnName, eventTarget) {
        if (this[fnName]) {
            const fn = this[fnName].bind(this);
            return fn(eventTarget);
        }
        else {
            if (this.hel.contextElement) {
                // See if the contextElement will handle this
                return this.hel.contextElement._execFunction(fnName, eventTarget);
            }
        }
        console.error(`'${fnName}' is not a member of this element or its hierarchy`, this);
        return null;
    }
}
window.customElements.define('fa-element', Element);