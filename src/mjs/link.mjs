import {ElementMixIn} from "./element_mixin.mjs";
import * as util from "./util.mjs";

export class Link extends ElementMixIn(HTMLAnchorElement) {
    // TODO: _linkAction: set, push, pop, goto
    // TODO: _linkContext: relative, absolute

    // TODO: Pop all by default
    // context-mode = Absolute vs Relative
    // page-mode = Visible vs Hidden
    // action = push, pop, set, goto
    // 

    // static get observedAttributes() {
    //     return [...super.observedAttributes, 'link-action'];
    // }

    // attributeChangedCallback(name, oldVal, newVal) {
    //     super.attributeChangedCallback(name, oldVal, newVal);
    //     if (name === 'link-action') {
    //         this._linkAction = newVal.toLowerCase();
    //     }
    // }

    constructor(template) {
        super();

        this.href = '';

        this.addEventListener('click', e => {
            e.preventDefault();

            this.href = this.getHref();

            const linkAction = this.getAttribute('data-fa-link-action');
            if (linkAction === 'push') {
                this.hel.stackRoot.push(this.href);
            }
            else if (linkAction === 'pop') {
                this.hel.stackRoot.pop();
            }
            else if (linkAction === 'popto') {
                const stackLevel = parseInt(this.getAttribute('data-fa-stack-level'));
                this.hel.stackRoot.popto(stackLevel);
            }
            else if (linkAction === 'set') {
                this.hel.stackRoot.clear(this.href);
            }
            else {
                // (stay on the same layer)
                this.hel.stackRoot.goto(this.href);
            }
        });

        this.addEventListener('contextmenu', e => {
            const linkAction = this.getAttribute('data-fa-link-action');
            // if (linkAction === 'pop') {
            //     e.preventDefault();
            //     this.hel.stackRoot.pop();
            //     return;
            // }
            this.href = this.getHref();
        });
    }

    _initHierarchy() {
        super._initHierarchy();
        if (!this.hel.stackRoot) {
            console.warn('Link not connected to a RenderStack', this);
        }
    }

    connectedCallback() {
        super.connectedCallback();
        this.href = this.getHref();
    }

    getHref() {
        let absolutePageState;
        const linkAction = this.getAttribute('data-fa-link-action');

        if (linkAction === 'pop') {
            const stackLevel = this.hel.stackRoot.layerStack.length - 2;
            if (stackLevel >= 0) {
                absolutePageState = this.hel.stackRoot.layerStack[stackLevel].pageState;
            }
        }
        else if (linkAction === 'popto') {
            const stackLevel = parseInt(this.getAttribute('data-fa-stack-level'));
            if (!isNaN(stackLevel)) {
                absolutePageState = this.hel.stackRoot.layerStack[stackLevel].pageState;
            }
        }
        else if (this.getAttribute('data-fa-link-context') === 'absolute') {
            absolutePageState = this._pageFilter;
        }
        else { // relative
            if (this.hel.stackRoot?.activeLayer) {
                absolutePageState = util.deepCopy(this.hel.stackRoot.activeLayer._pageState._external);
            }
            else {
                absolutePageState = util.deepCopy(this.global.app._pageState._external);
            }
            util.setPathValueArray(absolutePageState, this.absolutePageContext, this._pageFilter);
        }
        const hrefStr = util.pageStateToHashStr(absolutePageState);
        return `#${hrefStr}`;
    }
}
window.customElements.define('fa-link', Link, {extends: 'a'});


export class LocalLink extends ElementMixIn(HTMLElement) {
    constructor(template) {
        super();

        this.addEventListener('click', e => {
            this._execAction();
            e.preventDefault();
        });
    }

    connectedCallback() {
        super.connectedCallback();
        if (this.hasAttribute('active')) {
            this.removeAttribute('active');
            this._execAction();
        }
    }

    _execAction() {
        // Localize the link to the context element
        const internalState = util.setPathValueArray({}, this._relativePageContext, this._pageFilter);
        this.hel.contextElement.addInternalPageState(internalState);
    }
}
window.customElements.define('fa-local-link', LocalLink);


export class ToggleLink extends ElementMixIn(HTMLAnchorElement) {
    constructor(template) {
        super();
        
        this.href = '';
        this.addEventListener('click', e => {
            e.preventDefault();
            //console.error('TOGGLE LINK CLICK');
            this.href = this.getHref();
            this.hel.stackRoot.goto(this.href);
        });

        this.addEventListener('contextmenu', e => {
            this.href = this.getHref();
        });
    }

    connectedCallback() {
        super.connectedCallback();
        this.href = this.getHref();
    }

    _initHierarchy() {
        super._initHierarchy();
        if (!this.hel.stackRoot) {
            console.warn('Link not connected to a RenderStack', this);
        }
    }

    getHref() {
        const absolutePageState = this.getPageStateValue(!this.active);
        const hrefStr = util.pageStateToHashStr(absolutePageState);
        return `#${hrefStr}`;
    }

    getPageStateValue(bActive) {
        let absolutePageState;
        if (this.hel.stackRoot?.activeLayer) {
            absolutePageState = util.deepCopy(this.hel.stackRoot.activeLayer._pageState._external);
        }
        else {
            absolutePageState = util.deepCopy(this.global.app._pageState._external);
        }
        util.setPathValueArray(absolutePageState, this.absoluteTogglePath, bActive);
        return absolutePageState;
    }

    connectedCallback() {
        const localTogglePath = util.pathStrToArray(
            this.getAttribute('data-fa-page-toggle')
        );
        const pageFilter = util.parsePageFilterArray([
            ...localTogglePath,
            true
        ]);
        this.setAttribute('data-fa-page-filter', JSON.stringify(pageFilter));

        super.connectedCallback();
        this.absoluteTogglePath = [
            ...this.absolutePageContext,
            ...localTogglePath,
        ];
        // if (this.hasAttribute('active')) {
        //     this.removeAttribute('active');
        //     const absolutePageState = this.getPageStateValue(true);
        //     const hrefStr = util.pageStateToHashStr(absolutePageState);
        //     this.hel.stackRoot.goto(`#${hrefStr}`);
        // }
    }
}
window.customElements.define('fa-toggle-link', ToggleLink, {extends: 'a'});


export class LocalToggleLink extends ElementMixIn(HTMLAnchorElement) {
    constructor(template) {
        super();

        this.addEventListener('click', e => {
            this._execAction();
            e.preventDefault();
        });
    }

    connectedCallback() {
        this._localTogglePath = util.pathStrToArray(
            this.getAttribute('data-fa-page-toggle')
        );
        const pageFilter = util.parsePageFilterArray([
            ...this._localTogglePath,
            true
        ]);
        this.setAttribute('data-fa-page-filter', JSON.stringify(pageFilter));

        super.connectedCallback();
        if (this.hasAttribute('active')) {
            this.removeAttribute('active');
            this._execAction();
        }
    }

    _execAction() {
        const bActive = !this.active;
        const toggledState = util.parsePageFilterArray([
            ...this._localTogglePath,
            bActive
        ]);
        this.hel.contextElement.addInternalPageState(toggledState);
    }
}
window.customElements.define('fa-local-toggle-link', LocalToggleLink, {extends: 'a'});