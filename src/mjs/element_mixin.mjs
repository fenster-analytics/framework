import {PageState} from "./page_state.mjs";
import {global} from "./global.mjs";
import * as util from "./util.mjs";


export function findContextElement(thisNode) {
    let searchRoot = thisNode.parentNode;
    if (!searchRoot) {
        return global.app;
    }

    if (searchRoot.getRootNode) {
        searchRoot = searchRoot.getRootNode().host;
    }

    let el = searchRoot.closest('[is-fa-context]');
    if (el) {
        return el;
    }

    el = thisNode.getRootNode().host;
    if (el) {
        return el;
    }

    console.error(`no context element`, this);
    return null;
}


export const ElementMixIn = (Base) => class extends Base {
    static get observedAttributes() {
        return [
            'data-fa-page-filter',
            'data-fa-value-filter',
            'data-fa-page-context',
            'data-fa-page-state-internal',
        ];
    }

    attributeChangedCallback(name, oldVal, newVal) {
        if (name === 'data-fa-page-filter') {
            this._pageFilter = util.parsePageFilterStr(newVal);
        }
        else if (name === 'data-fa-value-filter') {
            this._valueFilter = newVal ? util.parseStrVal(newVal) : null;
        }
        else if (name === 'data-fa-page-context') {
            this._relativePageContext = newVal ? newVal.split('.') : [];
            this._updateAbsolutePageContext();
        }
        else if (name === 'data-fa-page-state-internal') {
            this._pageState.setInternal(newVal ? util.parseStrVal(newVal) : {});
        }
    }

    constructor(defaultPageState) {
        super();
        this.hel = {};
        this._connected = false;
        this._pageState = new PageState(defaultPageState);

        this._active = undefined;
    }

    disconnectedCallback() {
        this._connected = false;
    }

    connectedCallback() {
        this._connected = true;

        // attributeChangedCallback is called before connectedCallback
        // (Element will have access to all attribute by-products)
        this._initAttributes();
        this._initHierarchy();

        // Pull the initial pageState from the contextElement
        if (this.hel.contextElement) {
            this.pageState = this.hel.contextElement.pageState;
        }
        this._updateActiveFromFilter();
    }

    _initAttributes() {
        this.setAttribute('is-fa-element', '');
    }

    _initHierarchy() {
        this.hel.contextElement = this._findContextElement();
        this.hel.stackRoot = this._findStackRoot();
        this.hel.stackLayer = this._findStackLayer();
        this._updateAbsolutePageContext();
    }

    set pageState(external) {
        const isChanged = this._pageState.setExternal(external, this._relativePageContext);
        if (isChanged) {
            this._onPageStateChanged();
        }
    }

    get pageState() {
        return this._pageState.value;
    }

    get absolutePageContext() {
        return this._absolutePageContext;
    }

    addInternalPageState(addState) {
        const finalState = util.deepCopy(this._pageState._internal);
        util.deepAssign(finalState, addState);
        const isChanged = this._pageState.setInternal(finalState);
        if (isChanged) {
            this._onPageStateChanged();
        }
    }

    addExternalPageState(addState) {
        // TODO: If There's already a value at the location,
        // Merge the addState into it,
        // Rather than simply setting the pathValue

        // Get the base
        const absolutePageState = this.getRootPageState();
        console.log('addExternalPageState', absolutePageState, this._absolutePageContext, addState);

        // Build the new state node for this context
        const newState = {};

        // Get the node at this context
        const currentState = util.getPathValueArray(absolutePageState, this.absolutePageContext);
        if (currentState) {
            util.deepAssign(newState, currentState);
        }

        // Merge in the addState to make a newState
        util.deepAssign(newState, addState);
        
        // Set the new state at this context
        util.setPathValueArray(absolutePageState, this.absolutePageContext, newState);

        // Convert to href
        const pageStateStr = util.pageStateToHashStr(absolutePageState);
        const hrefStr = `#${pageStateStr}`;

        history.pushState(null, null, hrefStr);
        window.dispatchEvent(new HashChangeEvent("hashchange"));
    }

    getRootPageState() {
        if (this.hel.stackRoot?.activeLayer) {
            return util.deepCopy(this.hel.stackRoot.activeLayer.pageState);
        }
        else {
            return util.deepCopy(this.global.app.pageState);
        }
    }

    getAbsolutePageState() {
        return util.setPathValueArray({}, this.absolutePageContext, this._pageState._external);
    }

    _updateAbsolutePageContext() {
        if (!this._relativePageContext) {
            this._relativePageContext = [];
        }

        // Initialize global context in initHierarchy,
        // and update whenever pagecontext changes (which might be never)
        if (this.hel.contextElement) {
            this._absolutePageContext = [
                ...this.hel.contextElement.absolutePageContext,
                ...this._relativePageContext
            ];
        }
        else {
            this._absolutePageContext = this._relativePageContext;
        }
    }

    _onPageStateChanged() {
        //console.log('onPageStateChanged', this.pageState, this);
        this._updateActiveFromFilter();
    }

    _pullPageState(src) {
        console.assert(false, 'pullPageState is deprecated');
        //console.log('pullPageState', src.pageState, this);
        this.pageState = src.pageState;
    }

    _updateActiveFromFilter() {
        if (!this._connected) {
            return;
        }

        // Default to true if no filter
        if (!this._pageFilter && !this._valueFilter) {
            this.active = true;
        }
        else {
            this.active = this.evalFilter(this._pageFilter, this._valueFilter);
        }
    }

    evalFilter(pageFilter, valueFilter) {
        if (valueFilter) {
            const valueMatch = util.matchFilter(this.value, valueFilter);
            return valueMatch;
        }

        const isPageMatch = util.matchFilter(this.pageState, pageFilter);
        return isPageMatch;
    }

    set active(val) {
        if (val !== this._active) {
            this._active = val;
            if (val) {
                this.onActivate();
            }
            else {
                this.onDeactivate();
            }
        }
    }

    get active() {
        return this._active;
    }

    onActivate() {
        console.assert(this._connected, 'activated disconnected element');
        this.classList.add('active');
    }

    onDeactivate() {
        this.classList.remove('active');
    }

    get global() {
        return global;
    }

    _findContextElement() {
        return findContextElement(this);
    }

    _findStackRoot() {
        if (!this.hel.contextElement) {
            return null;
        }
        if (this.hel.contextElement.hel.stackRoot) {
            return this.hel.contextElement.hel.stackRoot;
        }
        if (this.hel.contextElement.tagName.toLowerCase() === 'fa-render-stack') {
            return this.hel.contextElement;
        }
        const sibStack = this.hel.contextElement._root.querySelector('fa-render-stack');
        if (sibStack) {
            return sibStack;
        }
        return null;
    }

    _findStackLayer() {
        // Find the renderTarget child of the renderStack
        // So,
        //  if the contextElement is a renderStack,
        //  and this is a renderLayer,
        //  then this is a stackLayer
        if (!this.hel.contextElement) {
            return null;
        }
        if (this.hel.contextElement.tagName.toLowerCase() === 'fa-render-stack') {
            if (this.tagName.toLowerCase() === 'fa-render-target') {
                return this;
            }
        }
        else if (this.hel.contextElement.hel.stackLayer) {
            return this.hel.contextElement.hel.stackLayer;
        }
        return null;
    }

    _findRootMatching(filterStr) {
        let el = this.hel.contextElement;
        while (el) {
            if (el.matches(filterStr)) {
                return el;
            }
            el = el.hel.contextElement;
        }
        return null;
    }
};