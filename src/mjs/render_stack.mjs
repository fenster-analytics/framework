import {FormElement} from "./form_element.mjs";
import {PageState} from "./page_state.mjs";
import * as util from "./util.mjs";

export class RenderStack extends FormElement {
    constructor() {
        super('fa_render_wrapper');

        // _shadowRoot is the original shadow root of the Application
        //   which contains all of the layers (fa-elements) for the pageStateStack
        // _root will just be a pointer to the active layer
        this._layerStack = [];
        this._activeLayer = undefined;

        // Convoluted approach with "self" so the listener can be removed when detached
        const self = this;
        this._keydownHandler = function(e) {
            //console.log('RenderStack: keydown', e.code, e.ctrlKey, e.altKey, e.shiftKey, e.metaKey);
            if (!self._activeLayer) {
                return;
            }

            if (e.ctrlKey) {
                if (e.code === 'Enter') {
                    self._activeLayer.dispatchEvent(new Event('execute', {bubbles: false, composed: false, cancelable: true}));
                    e.preventDefault();
                }
                else if (e.code === 'KeyS') {
                    if (e.shiftKey) {
                        self._activeLayer.dispatchEvent(new Event('publish', {bubbles: false, composed: false, cancelable: true}));
                        e.preventDefault();
                    }
                    else {
                        self._activeLayer.dispatchEvent(new Event('save', {bubbles: false, composed: false, cancelable: true}));
                        e.preventDefault();
                    }
                }
            }
            if (e.ctrlKey && e.shiftKey) {
                if (e.code === 'KeyF') {
                    console.log('hot key search');
                    //data-fa-page-context='modal'
                    //data-fa-page-filter='search'
                    // TODO: Move this functionality to the global app
                    // AddAbsolutePageState
                    const absolutePageState = FA.util.deepCopy(self.global.app._pageState._external);
                    FA.util.setPathValue(absolutePageState, 'modal', {'search': {'objectClass': 'chart'}});
                    const hrefStr = FA.util.pageStateToHashStr(absolutePageState);
                    const href = `#${hrefStr}`;
                    // goto (stay on the same layer)
                    history.pushState(null, null, href);
                    window.dispatchEvent(new HashChangeEvent("hashchange"));

                    self._activeLayer.dispatchEvent(new Event('findAll', {bubbles: false, composed: false, cancelable: true}));
                    e.preventDefault();
                }
            }
        };
        document.addEventListener('keydown', this._keydownHandler);
    }

    disconnectedCallback() {
        super.disconnectedCallback();
        document.removeEventListener('keydown', this._keydownHandler);
    }

    _initAttributes() {
        super._initAttributes();
        this.setAttribute('data-fa-bind-value', '');
    }

    _findValueRoot() {
        // Render Stacks don't require data-fa-bind-value,
        // but might want to change this
        return this.hel.contextElement._findValueRoot();
    }

    setTemplateAndRender(t) {
        console.log('RenderStack::setTemplateAndRender', t);
        this._layerTemplate = t;
        // Don't render
    }

    get renderContext() {
        return this.hel.contextElement.renderContext;
    }

    get layerStack() {
        return this._layerStack;
    }

    get activeLayer() {
        return this._activeLayer;
    }

    registerBreadcrumb(b) {
        this._breadcrumb = b;
    }

    updateBreadcrumb() {
        if (this._breadcrumb) {
            this._breadcrumb._updateStack();
        }
    }

    getValueElements() {
        return [];
    }

    getFormElements() {
        if (this._activeLayer) {
            return [this._activeLayer];
        }
        return [];
    }

    getPageStateElements() {
        if (this._activeLayer) {
            return [this._activeLayer];
        }
        return [];
    }

    connectedCallback() {
        super.connectedCallback();

        // Create the first layer (could be deferred to onActivate)
        // TODO: Ideally wait until page state change?
        // Otherwise this will create the first layer with an empty pageState
        // and then refresh immediately with the current pageState
        console.assert(this._layerStack.length === 0);
        const initPageState = this.pageState;
        this.push();
        this._activeLayer.pageState = initPageState;
    }

    push(hrefStr) {
        // Convert absolute pageState to string
        if (hrefStr && typeof(hrefStr) === 'object') {
            hrefStr = '#' + util.pageStateToHashStr(hrefStr);
        }

        // Deactivate all other instances
        this._activeLayer = null;
        for (const layer of this._layerStack) {
            layer.active = false;
        }

        // Set the new page state
        history.pushState(null, null, hrefStr);
        window.dispatchEvent(new HashChangeEvent("hashchange"));

        const layer = this.createLayer();
        layer.setAttribute('data-fa-layer-index', this._layerStack.length);

        this._layerStack.push(layer);
        this.activateLayer(layer);

        this.dispatchEvent(new Event('push', {bubbles: false, composed: false}));
        return layer;
    }

    pop() {
        if (this._layerStack.length <= 1) {
            console.warn('RenderStack: no more layers to pop to');
            return false;
        }

        // Remove the current layer
        const popLayer = this._layerStack.pop();
        this.removeLayer(popLayer);

        // Load the previous layer
        const layer = this._layerStack.slice(-1)[0];
        this.activateLayer(layer);

        // Hack: Restore external pageState to window
        // (should back-propagate up to app ideally)
        const hrefStr = util.pageStateToHashStr(layer.pageState);
        history.replaceState(null, null, `#${hrefStr}`);
        //this.global.app.pageState = this.global.app.getPageStateFromLocation();
        //this.global.app._onPageStateChanged();
        this.dispatchEvent(new Event('pop', {bubbles: false, composed: false}));

        return layer;
    }

    popto(layerIndex) {
        if (layerIndex > this._layerStack.length - 2
            || layerIndex < 0) {
            console.warn('RenderStack: popto layerIndex out of bounds:', layerIndex);
            return false;
        }

        // Pop layers until layerIndex is reached
        for (let idx=this._layerStack.length - 1; idx>layerIndex; idx--) {
            const popLayer = this._layerStack.pop();
            this.removeLayer(popLayer);
        }

        // Load the target layer
        const layer = this._layerStack[layerIndex];
        this.activateLayer(layer);

        // Hack: Restore external pageState to window
        // (should back-propagate up to app ideally)
        const hrefStr = util.pageStateToHashStr(layer.pageState);
        history.replaceState(null, null, `#${hrefStr}`);
        //this.global.app.pageState = this.global.app.getPageStateFromLocation();
        //this.global.app._onPageStateChanged();

        return layer;
    }

    clear(hrefStr) {
        // Convert absolute pageState to string
        if (hrefStr && typeof(hrefStr) === 'object') {
            hrefStr = '#' + util.pageStateToHashStr(hrefStr);
        }

        // Deactivate all other instances
        this._activeLayer = null;
        while (this._layerStack.length > 1) {
            const popLayer = this._layerStack.pop();
            this.removeLayer(popLayer);
        }

        // Load the remaining layer
        const layer = this._layerStack.slice(-1)[0];
        this.activateLayer(layer);

        // Set the new page state
        history.pushState(null, null, hrefStr);
        window.dispatchEvent(new HashChangeEvent("hashchange"));

        return layer;
    }

    goto(hrefStr) {
        history.pushState(null, null, hrefStr);
        window.dispatchEvent(new HashChangeEvent("hashchange"));
    }

    removeLayer(layer) {
        console.warn('RenderStack: @todo: check for unsaved changes');
        layer.removed();
        layer.active = false;
        this._shadowRoot.removeChild(layer);
    }

    activateLayer(layer) {
        // Load the pageState from the layer (even though we'll write to it later)
        this.pageState = layer.pageState;
        layer.active = true;
        this._activeLayer = layer;
        //this._activeLayer.dispatchEvent(new Event("uncover", {bubbles: false, cancelable: false}));
    }

    createLayer() {
        const layer = document.createElement('fa-render-target');
        layer.template = this._layerTemplate;
        this._shadowRoot.appendChild(layer);
        return layer;
    }
}
window.customElements.define('fa-render-stack', RenderStack);