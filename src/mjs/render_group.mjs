import {Element} from "./element.mjs";
import {RenderSwitch} from "./render_switch.mjs";
import * as util from "./util.mjs";

export class RenderGroup extends RenderSwitch {
    constructor() {
        super();
        this._childElementType = 'fa-render-target';
    }

    // Only difference from switch is that value should be propagated to all children
    // Rather than only active layer
    getFormElements() {
        return this._root.querySelectorAll(this._childElementType);
    }
}
window.customElements.define('fa-render-group', RenderGroup);