import {FormElement} from "./form_element.mjs";
import * as util from "./util.mjs";

const sampleData = [
    { category: "Fruits", price: "$1", stocked: true, name: "Apple" },
    { category: "Fruits", price: "$1", stocked: true, name: "Dragonfruit" },
    { category: "Fruits", price: "$2", stocked: false, name: "Passionfruit" },
    { category: "Vegetables", price: "$2", stocked: true, name: "Spinach" },
    { category: "Vegetables", price: "$4", stocked: false, name: "Pumpkin" },
    { category: "Vegetables", price: "$1", stocked: true, name: "Peas" }
];

export class Example extends FormElement {
    constructor() {
        // "docs_example" will render using the template: docs/example.html
        super('docs_example');
        this._productList = sampleData;
        this._updateList();
    }

    filterName(el) {
        this._filterStr = el.value;
        this._updateList();
    }

    filterStock(el) {
        // getElementValue converts checkbox value to true/false
        this._inStockOnly = util.getElementValue(el);
        this._updateList();
    }

    _updateList() {
        const categoryDict = {};
        for (const product of this._productList) {
            // Skip out-of-stock products
            if (this._inStockOnly && !product.stocked) {
                continue;
            }

            // Skip names that don't pass filter
            if (this._filterStr) {
                if (!product.name?.toLowerCase().includes(this._filterStr)) {
                    continue;
                }
            }

            // Logic should generally happen outside of the template
            product.displayClass = product.stocked ? 'in-stock' : 'not-in-stock';

            // Get or create the list for products in this category
            const category = util.getOrSetPathValue(categoryDict, product.category, Array);
            category.push(product);
        }
        this.shared.categoryDict = categoryDict;
        this._renderDynamicContent();
    }
}
window.customElements.define('fa-docs-example', Example);