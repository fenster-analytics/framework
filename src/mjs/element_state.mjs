export class ElementState {
    constructor(key, index, stateMachine, config, track) {
        this._key = key;
        this._index = index;
        this._stateMachine = stateMachine;
        this._config = config;
        this._track = track;
        this._startTimeMS = undefined;
        this._totalTimeMS = 0;
        this._startCount = 0;
        this._init();
    }

    _init() {
        // Custom setup. E.g. set a template directly
    }

    set template(t) {
        this._defaultTemplate = t;
    }

    get template() {
        if (this._config && this._config.template) {
            return this._config.template;
        }
        if (this._defaultTemplate) {
            return this._defaultTemplate;
        }
    }

    get forceRender() {
        return this._config?.forceRender;
    }

    set local(jsonVal) {
        this._stateMachine.local = jsonVal;
    }

    set value(v) {
        this._stateMachine.value = v;
    }

    get value() {
        return this._stateMachine.value;
    }

    set valueMask(vm) {
        this._stateMachine.valueMask = vm;
    }

    get valueMask() {
        return this._stateMachine.valueMask;
    }

    getAttribute(attrStr) {
        return this._stateMachine.getAttribute(attrStr);
    }

    setAttribute(attrStr, val) {
        this._stateMachine.setAttribute(attrStr, val);
    }

    get shared() {
        return this._stateMachine._shared;
    }

    get global() {
        return this._stateMachine.global;
    }

    get hel() {
        return this._stateMachine.hel;
    }

    get el() {
        return this._stateMachine.el;
    }

    get pageState() {
        return this._stateMachine.pageState;
    }

    set pageState(p) {
        this._stateMachine.pageState = p;
    }

    get root() {
        return this._stateMachine._root;
    }

    async _start() {
        this._startCount++;
        this._startTimeMS = Date.now();
        await this.start();
    }

    async _update() {
        //console.log('ElementState: update', this._key);
        await this.update();
    }

    async _stop() {
        const elapsedTimeMS = Date.now() - this._startTimeMS;
        this._totalTimeMS += elapsedTimeMS;
        //console.log('ElementState: stop', this._key, elapsedTimeMS);
        await this.stop();
    }

    _execFunction(fnName, eventTarget) {
        if (this[fnName] === undefined) {
            return undefined;
        }

        // Trigger the function by name, pass in the instigator
        const fn = this[fnName].bind(this);
        return fn(eventTarget);
    }

    getStats() {
        return {
            'start_count': this._startCount,
            'total_time_ms': this._totalTimeMS,
        }
    }
    
    //
    // StateMachine commands
    //

    spawn(key) {
        if (Array.isArray(key)) {
            for (k of key) {
                this._stateMachine.spawnState(k);
            }
        }
        else {
            this._stateMachine.spawnState(key);
        }
        this._stateMachine._requestUpdate();
    }

    kill() {
        this._stateMachine.killState(this._key);
        this._stateMachine._requestUpdate();
    }

    goto(key, message=undefined) {
        this._stateMachine.killState(this._key);
        this._stateMachine.shared.tMsg = message;
        this._stateMachine.spawnState(key);
        this._stateMachine._requestUpdate();
    }

    killAll(track=undefined) {
        this._stateMachine.killAllStates(track);
        this._stateMachine._requestUpdate();
    }

    //
    // Virtual methods
    //

    async start() { }
    async update() { }
    async stop() { }
}


export class TestPromiseElementState extends ElementState {
    async start() {
        this.test = test.testPromise(this._config.delay).then(() =>
            this.goto(this._config.next)
        );
        console.log('ASYNC TEST');
    }
}