import {ElementStateMachine, defaultStateMachineDef} from "./element_state_machine.mjs";
import {ElementState} from "./element_state.mjs";
import {PageState} from "./page_state.mjs";
import {global} from "./global.mjs";
import * as util from "./util.mjs";

export class Application extends ElementStateMachine {
    constructor(stateMachineDef, defaultPageState) {
        if (!stateMachineDef) {
            stateMachineDef = defaultStateMachineDef;
        }
        super(stateMachineDef, defaultPageState);
        global.app = this;

        window.addEventListener('hashchange', () => {
            const externalPageState = this.getPageStateFromLocation();
            console.log('HASH CHANGE', externalPageState);
            this.pageState = externalPageState;
        });
        this.pageState = this.getPageStateFromLocation();

        // Load the local options, like theme, font mods, etc
        global.initLocalOptions();
    }

    set pageState(external) {
        // Application should always trigger pageState change
        // note: this won't work on unchanged states
        // if there's an intermediate layer between app and renderStack
        const isChanged = this._pageState.setExternal(external, this._pageContextRelative);
        this._onPageStateChanged();
    }

    get pageState() {
        return super.pageState;
    }

    getPageStateFromLocation() {
        const hashStr = document.location.hash.substring(1);
        if (hashStr) {
            const hashStrPageState = util.hashStrToPageState(hashStr);
            return hashStrPageState;
        }

        return {};
    }

    _findContextElement() {
        // Application is the outermost element
        return null;
    }

    _findStackRoot() {
        return null;
    }
}
