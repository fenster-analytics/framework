import {Application} from "./application.mjs";
import {ElementState} from "./element_state.mjs";

class Init extends ElementState {
    async start() {
        console.log('Docs: Init');
        this.goto('main');
    }
}

class Main extends ElementState {
    async start() {
        console.log('Docs: Main');
    }
}

const stateMachineDef = {
    'init': {
        'class': Init,
        'active': true,
        'config': {
            'template': 'fa_loading',
        }
    },
    'main': {
        'class': Main,
        'config': {
            'template': 'docs_main',
        }
    },
}

export class Docs extends Application {
    constructor() {
        const defaultPageState = {
            'main': 'home',
        };
        super(stateMachineDef, defaultPageState);
    }

    setValue1(el) {
        this.value = {'t': 'val1', 'config': {'test': 1}};
    }

    setValue2(el) {
        this.value = {'t': 'val2', 'config': {'test': 2}};
    }

    getValue() {
        const v = this.value;
        console.log('value:', v);
    }
}
window.customElements.define('fa-docs', Docs);