import {FormElement} from "./form_element.mjs";
//import {Element} from "./element.mjs";
import * as util from "./util.mjs";

export class RenderTarget extends FormElement {
    _updateActiveFromFilter() {
        // Override fa-element to only allow manual activation/deactivation
        // of the RenderTarget itself

        // Check if any sub elements should update though
        this._activateManualPageStateElements();
    }

    _findValueRoot() {
        // Use the valueRoot from the Stack, Group, or Switch
        return this.hel.contextElement._findValueRoot();
    }

    get renderContext() {
        return this.hel.contextElement.renderContext;
    }

    get lowerLayer() {
        if (this.hel.stackRoot && this.hasAttribute('data-fa-layer-index')) {
            const idx = parseInt(this.getAttribute('data-fa-layer-index'));
            if (idx > 0) {
                const lowerLayer = this.hel.stackRoot.layerStack[idx - 1];
                return lowerLayer;
            }
        }
        return null;
    }

    set onRemoved(callbackFn) {
        console.assert(!this._onRemovedFn, 'Layer removed callback already registered');
        this._onRemovedFn = callbackFn;
    }

    removed() {
        if (this._onRemovedFn) {
            this._onRemovedFn(this);
        }
        this.dispatchEvent(new Event('pop', {bubbles: false, composed: false}));
    }
}
window.customElements.define('fa-render-target', RenderTarget);