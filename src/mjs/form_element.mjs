import {Element, contextType} from "./element.mjs";
import * as util from "./util.mjs";


export class FormElement extends Element {
    _initAttributes() {
        super._initAttributes();
        this.setAttribute('is-fa-form', '');
    }

    evalFilter(pageFilter, valueFilter) {
        if (valueFilter) {
            const value = this._value;
            //console.log('FORM ELEMENT EVAL FILTER', value, valueFilter);
            if (value !== undefined) {
                const isValueMatch = util.matchFilter(value, valueFilter);
                if (isValueMatch) {
                    return true;
                }
            }
            return false;
        }
        return super.evalFilter(pageFilter, valueFilter);
    }

    get value() {
        this._value = this.getRootValue(this.hel.valueContext);
        return this._value;
    }

    set value(jsonVal) {
        // For pop-up forms (like parameter editor)
        // Where the value is set directly on the created element
        if (!this.hel.valueRoot) {
            this._value = jsonVal;
            return;
        }

        this.hel.valueRoot.setRootValue(this.hel.valueContext, jsonVal);
        this.hel.valueRoot.commitRootValueChanges();
        this._value = this.getRootValue(this.hel.valueContext);
        this._pureValue = this._value;
        //console.warn('set value', this._value);

        this.checkPurity();
    }

    // get valueMask() {
    //     return this._valueMask;
    // }

    // setMaskedValue(value, valueMask) {
    //     this._valueMask = valueMask;
    //     this.value = value;
    // }

    set disabled(d) {
        if (d) {
            this.setAttribute('disabled', 'disabled');
        }
        else {
            this.removeAttribute('disabled');
        }
        for (const el of this.getBindValueElements()) {
            el.disabled = d;
        }
    }

    getRootValue(valueContext) {
        return util.getPathValueArray(this.hel.valueRoot._value, valueContext);
    }

    setRootValue(valueContext, value) {
        // TODO: Set the pending value
        // Only allowed on the valueRoot
        console.assert(this === this.hel.valueRoot, `setRootValue called on non-valueRoot`);
        if (typeof(this._pendingValue) !== 'object') {
            console.warn('Converting pendingValue to dict from', this._pendingValue, this);
            this._pendingValue = {};
        }
        util.setPathValueArray(this._pendingValue, valueContext, value);
    }

    deleteRootValue(valueContext) {
        console.assert(this === this.hel.valueRoot, `deleteRootValue called on non-valueRoot`);
        util.setPathValueArray(this._pendingValue, valueContext, undefined);
    }

    _initHierarchy() {
        super._initHierarchy();

        if (this.isValueRoot()) {
            this.hel.valueContext = [];

            // Value could have been set already by attribute
            if (this._value === undefined) {
                this._value = {};
            }

            // All uncommitted changes accumulate in pendingValue
            this._pendingValue = util.deepCopy(this._value);
        }
        else {
            // Don't return any values (haven't rendered, can't be scraped)
            this._value = undefined;
        }
    }

    hasChange(valueContext) {
        if (this.hel.valueRoot._forceHasChange) {
            // @hack: used by Strata editor & properties
            // to re-render when valueMask changes
            return true;
        }
        if (this.hel.valueRoot._valueDiffTree === undefined) {
            return true;
        }

        const hasDiff = util.getPathValueArray(this.hel.valueRoot._valueDiffTree, valueContext)
        return Boolean(hasDiff);
    }

    commitRootValueChanges(recursion=0) {
        if (recursion > 10) {
            console.error('recursion depth exceeded', recursion, this);
        }
        console.assert(this === this.hel.valueRoot, `commitRootValueChanges called on non-valueRoot`);

        // Check for value changes (which should be re-rendered)
        this._valueDiffTree = util.makeDiffTree(this._value, this._pendingValue);
        const bHasChanges = Boolean(this._valueDiffTree);
        //!util.deepEquals(this._value, this._pendingValue);
        //console.log('commitRootValueChanges: hasChanges:', bHasChanges, this._valueDiffTree, recursion);

        this._value = this._pendingValue;
        this._pendingValue = util.deepCopy(this._value);

        if (bHasChanges) {
            this._renderDynamicContent(recursion);
        }
        else {
            // Reached after the final commit recursion
            // (when no more changes are being made by rendering)
            this.checkPurity();
            this._lastCommitTS = Date.now();
        }
    }

    checkPurity() {
        const bIsPure = util.deepEquals(this._value, this._pureValue);
        if (bIsPure) {
            this._root.querySelectorAll('[disable-on-pure]').forEach(
                el => el.setAttribute('disabled', true)
            );
            this._root.querySelectorAll('[enable-on-pure]').forEach(
                el => el.removeAttribute('disabled')
            );
        }
        else {
            this._root.querySelectorAll('[disable-on-dirty]').forEach(
                el => el.setAttribute('disabled', true)
            );
            this._root.querySelectorAll('[enable-on-dirty]').forEach(
                el => el.removeAttribute('disabled')
            );
        }
    }


    getBindValueElements() {
        return this._root.querySelectorAll('[data-fa-bind-value]');
    }

    getKeyElements() {
        return this._root.querySelectorAll('[data-fa-bind-key]');
    }

    getValueElements() {
        return this._root.querySelectorAll('[data-fa-bind-value]:not([is-fa-form])');
    }

    // getGroupElements() {
    //     return this._root.querySelectorAll('[data-fa-bind-value][is-fa-group]');
    // }

    getFormElements() {
        return this._root.querySelectorAll('[data-fa-bind-value][is-fa-form]');
    }

    getValueMapElements() {
        return this._root.querySelectorAll('[data-fa-map-value]');
    }

    _renderDynamicContent(recursion=0) {
        // The value (from the root) at the context for this Element
        this._value = this.getRootValue(this.hel.valueContext);
        this._updateActiveFromFilter();

        //console.log('renderDynamicContent', recursion);
        super._renderDynamicContent();

        // For debugging
        //this.setAttribute('data-fa-value', JSON.stringify(this._value));

        // Propagate keys (used by Strata parameter editor)
        for (const el of this.getKeyElements()) {
            const key = el.hel.valueContext.slice(-1)[0];
            el.value = key;
        }

        // Propagate value to immediate children
        for (const el of this.getValueElements()) {
            const elementValueContext = this._getElementValueContext(el);

            // If dynamic has already been rendered once, don't re-render if value is unchanged
            if (this._renderedDynamic && !this.hasChange(elementValueContext.path)) {
                //console.log('skipping unchanged val el', recursion, elementValueContext.path, this._valueDiffTree, this.hel.valueRoot._valueDiffTree);
                continue;
            }

            const localValue = this.getRootValue(elementValueContext.path);
            if (localValue === undefined) {
                continue;
            }

            if (elementValueContext.type === contextType.list) {
                console.assert(Array.isArray(localValue), elementValueContext.path, 'is not array:', localValue);
                el.checked = localValue.includes(el.value);
            }
            else if (elementValueContext.type === contextType.dict) {
                console.warn('TODO: Set dict');
                console.assert(typeof(localValue) === 'object', elementValueContext.path, 'is not object:', localValue);
            }
            else if (elementValueContext.type === contextType.group) {
                const elValue = el.value;
                const isChecked = (elValue === localValue);
                util.setElementValue(el, isChecked);
            }
            else {
                util.setElementValue(el, localValue);
            }
        }

        this.propagateValueMap();

        // Notify child elements that they may want to re-render
        for (const el of this.getFormElements()) {
            const elementValueContext = this._getElementValueContext(el);

            // If dynamic has already been rendered once, don't re-render if value is unchanged
            if (this._renderedDynamic && !this.hasChange(elementValueContext.path)) {
                //console.log('skipping unchanged form el', recursion, elementValueContext.path, this._valueDiffTree, this.hel.valueRoot._valueDiffTree);
                continue;
            }
            el._renderDynamicContent();

            // const diffNode = util.getPathValueArray(this.hel.valueRoot._valueDiffTree, elementValueContext.path);
            // const bChanged = !!diffNode;
            // if (bChanged) {
            //     console.log('RENDER SUB FORM', )
            //     el._renderDynamicContent();
            // }
        }

        // Check for post-Render changes (like inputs with default options)
        if (this.isValueRoot()) {
            this._collectValueChanges();
            this.commitRootValueChanges(recursion + 1);
        }

        this._renderedDynamic = true;
    }

    propagateValueMap() {
        //console.log('PROPAGATE VALUE MAP', this._value);
        // Transform values for map elements (generally for viewers nested in editors)
        // (these values are not read back from elements)
        for (const el of this.getValueMapElements()) {
            const valueMap = JSON.parse(el.getAttribute('data-fa-map-value'));
            //console.error('VALUE MAP', valueMap);
            const value = {};
            for (const [localKey, pathStr] of Object.entries(valueMap)) {
                if (pathStr === null) {
                    value[localKey] = null;
                }
                else {
                    value[localKey] = util.getPathValue(this._value, pathStr, null);
                }
            }
            //console.warn('SET VALUE FROM VALUE MAP', value);
            el.value = value;
        }
    }

    _collectValueChanges() {
        // Assign single values at a time, rather than assigning objects and stomping
        // All internal elements update the root value
        for (const el of this.getValueElements()) {
            this._collectElementValue(el);
        };

        // All sub-elements update the root value
        for (const el of this.getFormElements()) {
            //console.log('COLLECT FORM VALUE', el, this);
            el._collectValueChanges();
        };
    }

    _getElementValueContext(el) {
        let contextStr = el.getAttribute('data-fa-bind-value');
        let contextPath;
        let valueType;

        let parentContext = this.hel.valueContext;
        let upLevel = 0;

        if (contextStr) {
            // Allow relative pathing
            //if (el.hel.valueRoot !== this) {
                const upLevelMatches = contextStr.match(/^\.+/);
                if (upLevelMatches) {
                    upLevel = upLevelMatches[0].length - 1;
                    if (parentContext.length < upLevel) {
                        console.error(`${contextStr} outside of parentContext:`, parentContext, this);
                        return;
                    }
                    parentContext = [...parentContext];
                    parentContext.splice(0, upLevel);
                    contextStr = contextStr.substring(upLevel + 1);
                }
            //}

            if (contextStr.endsWith('[]')) {
                valueType = contextType.list;
                contextStr = contextStr.slice(0, -2);
            }
            else if (contextStr.endsWith('{}')) {
                valueType = contextType.list;
                contextStr = contextStr.slice(0, -2);
            }
            else if (el.hasAttribute('is-fa-group')) {
                valueType = contextType.group;
            }
            else {
                valueType = contextType.value;
            }
            contextPath = contextStr.split('.');
        }
        else {
            contextPath = [];
            valueType = contextType.value;
        }

        const fullContextPath = [
                ...parentContext,
                ...contextPath,
            ];
        return {
            'type': valueType,
            'path': fullContextPath,
            'level': upLevel,
        }
    }

    _handleChangeEvent(event) {
        super._handleChangeEvent(event);
        const el = event.target;

        // const valueContext = this._getElementValueContext(el);
        // if (valueContext.level > 0) {
        //     // Need to strip the depth from the attribute
        //     console.log('SENDING IT UP TO', el.hel.valueRoot.hel.valueRoot);
        //     el.hel.valueRoot.hel.valueRoot._handleChangeEvent(event);
        //     return;
        // }
        // if (el.hel.valueRoot !== this) {
        //     el.hel.valueRoot._handleChangeEvent(event);
        //     return;
        // }

        // Update the rootValue
        if (el.hasAttribute('data-fa-bind-value')) {
            if (el.hasAttribute('is-fa-form')) {
                el._collectValueChanges();
            }
            else {
                this._collectElementValue(el);
            }
            this.hel.valueRoot.commitRootValueChanges();
            this.dispatchEvent(new Event("change", {bubbles: true, cancelable: true}));
        }
    }

    _collectElementValue(el) {
        const valueContext = this._getElementValueContext(el);

        if (valueContext.type === contextType.list) {
            // Have to collect all element values in group
            const groupBindValue = el.getAttribute('data-fa-bind-value');
            const groupList = this._root.querySelectorAll(`[data-fa-bind-value="${groupBindValue}"]`);
            const value = Array.from(groupList).filter(el => el.checked).map(el => el.value);
            this.hel.valueRoot.setRootValue(valueContext.path, value);
        }
        else if (valueContext.type === contextType.group) {
            // Only collect the value of one checked item
            const groupBindValue = el.getAttribute('data-fa-bind-value');
            const groupList = this._root.querySelectorAll(`[data-fa-bind-value="${groupBindValue}"]`);
            const value = Array.from(groupList).filter(el => el.checked).map(el => el.value);
            this.hel.valueRoot.setRootValue(valueContext.path, value[0]);
        }
        else {
            const value = util.getElementValue(el);
            this.hel.valueRoot.setRootValue(valueContext.path, value);
        }
    }
}
window.customElements.define('fa-form-element', FormElement);