class GlobalObject {
    constructor() {
        this._app = null;
        this._renderData = {};
        this.spinnerHTML = `[Loading Spinner]`;
        this.loaderHTML = `Loading...`;
        this.waitingHTML = `Waiting...`;
    }
    set app(v) {
        this._app = v;
    }
    get app() {
        return this._app;
    }

    get renderData() {
        return this._renderData;
    }

    initLocalOptions() {
        // TODO: List of options and defaults?
        // Set the current theme
        const initTheme = localStorage.getItem('option-theme');
        if (initTheme) {
            // TODO: data-bind-local-storage
            const el = document.querySelector('[data-option-key="theme"]');
            if (el) {
                el.value = initTheme;
            }
            document.documentElement.setAttribute(`data-theme`, initTheme);
        }
    }

    changeLocalOption(e) {
        const el = e.target;
        const key = el.getAttribute('data-fa-option-key');
        const val = el.value;
        console.log(`set local option: ${key} = ${val}`);
        document.documentElement.setAttribute(`data-${key}`, val);
        localStorage.setItem(`option-${key}`, val);
    }
}
export var global = new GlobalObject();