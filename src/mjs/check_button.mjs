import {Element} from "./element.mjs";
import * as util from "./util.mjs";

export class CheckButton extends Element {
    constructor() {
        super('fa_check_button');
    }

    _initAttributes() {
        super._initAttributes();
        this.setAttribute('is-fa-group', '');
    }

    connectedCallback() {
        super.connectedCallback();

        this.isArrayValue = (this.hasAttribute('data-fa-bind-value')
            && this.getAttribute('data-fa-bind-value').endsWith('[]'));

        this.addEventListener('click', e => {
            if (this.hasAttribute('disabled')) {
                return;
            }

            if (this.isArrayValue) {
                // Deselect others if not Ctrl+click
                if (!(e.ctrlKey || e.metaKey)) {
                    this.deselectGroup();
                }
            }
            else {
                // Always deselect others in group
                this.deselectGroup();
            }
            this.checked = !this.checked;
            this.dispatchEvent(new Event("change", {bubbles: true, cancelable: true}));
        });
    }

    set value(v) {
        this._value = v;
    }

    get value() {
        return this._value;
    }

    get checked() {
        return this.hasAttribute('checked');
    }

    set checked(newChecked) {
        const previousChecked = this.checked;
        if (newChecked === previousChecked) {
            return;
        }

        if (newChecked) {
            this.setAttribute('checked', true);
        }
        else {
            this.removeAttribute('checked');
        }
    }

    set disabled(d) {
        if (d) {
            this.setAttribute('disabled', 'disabled');
        }
        else {
            this.removeAttribute('disabled');
        }
    }

    deselectGroup() {
        const contextStr = this.getAttribute('data-fa-bind-value');
        if (contextStr === null) {
            return;
        }

        const groupList = this.hel.contextElement._root.querySelectorAll(`[data-fa-bind-value="${contextStr}"]`);
        for (const el of groupList) {
            el.checked = false;
        }
    }
}
window.customElements.define('fa-check-button', CheckButton);