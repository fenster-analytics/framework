import {Element} from "./element.mjs";
import {PageState} from "./page_state.mjs";
import * as util from "./util.mjs";

export class Breadcrumb extends Element {
    constructor() {
        super('fa_breadcrumb');
        this._initOptionList();
    }

    _initOptionList() {
        if (this._optionList !== undefined) {
            return;
        }

        this._optionList = [];
        for (const el of this._slotChildren) {
            const pageFilterStr = el.getAttribute('data-fa-page-filter');
            const pageFilter = util.parsePageFilterStr(pageFilterStr);

            const optionDef = {
                'pageFilter': pageFilter,
                'display': Handlebars.compile(el.innerHTML),
            };
            this._optionList.push(optionDef);
        }

        // Free the child elements
        this._slotChildren = null;
    }

    _initHierarchy() {
        super._initHierarchy();
        if (this.hel.stackRoot) {
            this.hel.stackRoot.registerBreadcrumb(this);
        }
        else {
            console.warn('Breadcrumb not connected to a RenderStack', this);
        }
    }

    _onPageStateChanged() {
        super._onPageStateChanged();
        this._updateStack();
    }

    _updateStack() {
        if (!this.hel.stackRoot) {
            console.error('Breadcrumb: no stackRoot', this);
            return;
        }

        const stackData = [];
        const layerStack = this.hel.stackRoot.layerStack;

        for (const [idx, el] of layerStack.entries()) {
            //console.log('Breadcrumb layer', idx, el.pageState, el.shared, el);
            const optionDef = this._findOption(el.pageState);
            if (!optionDef) {
                console.warn('Breadcrumb option not found for', el.pageState, this);
                continue;
            }
            const displayFn = optionDef.display;
            //const pageStateStr = util.pageStateToHashStr(el.getAbsolutePageState());
            //const hrefStr = `#${pageStateStr}`;
            const absolutePageState = el.getAbsolutePageState();
            stackData.push({
                'idx': idx,
                'display': displayFn(el.shared),
                'filter': JSON.stringify(absolutePageState),
            })
        }

        // Set the document title
        if (stackData.length > 0) {
            const stackTop = stackData.slice(-1)[0];
            document.title = stackTop.display;
        }

        // TODO: Have renderDynamic render the template to a target,
        // rather than relying on the compiled data bind

        this.shared.stack = stackData;
        this.shared.hasClose = (stackData.length > 1);
        this._renderDynamicContent();
    }

    _findOption(pageState) {
        const localPageState = util.getPathValueArray(pageState, this._relativePageContext, {});
        for (const def of this._optionList) {
            if (util.matchFilter(localPageState, def.pageFilter)) {
                return def;
            }
        }
        console.warn('Breadcrumb: no option found for', pageState.value, this);
        return null;
    }
}
window.customElements.define('fa-breadcrumb', Breadcrumb);